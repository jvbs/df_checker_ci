<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Security Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/security_helper.html
 */

// ------------------------------------------------------------------------
if ( ! function_exists('xss_clean'))
{
	/**
	 * XSS Filtering
	 *
	 * @param	string
	 * @param	bool	whether or not the content is an image file
	 * @return	string
	 */
	function xss_clean($str, $is_image = FALSE)
	{
		return get_instance()->security->xss_clean($str, $is_image);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('sanitize_filename'))
{
	/**
	 * Sanitize Filename
	 *
	 * @param	string
	 * @return	string
	 */
	function sanitize_filename($filename)
	{
		return get_instance()->security->sanitize_filename($filename);
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('do_hash'))
{
	/**
	 * Hash encode a string
	 *
	 * @todo	Remove in version 3.1+.
	 * @deprecated	3.0.0	Use PHP's native hash() instead.
	 * @param	string	$str
	 * @param	string	$type = 'sha1'
	 * @return	string
	 */
	function do_hash($str, $type = 'sha1')
	{
		if ( ! in_array(strtolower($type), hash_algos()))
		{
			$type = 'md5';
		}

		return hash($type, $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('strip_image_tags'))
{
	/**
	 * Strip Image Tags
	 *
	 * @param	string
	 * @return	string
	 */
	function strip_image_tags($str)
	{
		return get_instance()->security->strip_image_tags($str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('encode_php_tags')){
	/**
	 * Convert PHP tags to entities
	 *
	 * @param	string
	 * @return	string
	 */
	function encode_php_tags($str){
		return str_replace(array('<?', '?>'), array('&lt;?', '?&gt;'), $str);
	}
}

if (!function_exists('antinjection')){
	function antinjection($str){
		$str = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/","",$str);
		$str = trim($str);
		$str = strip_tags($str);
		$str = addslashes($str);
		$str = str_replace("'", " ", $str);
		return str_replace(trim("\ "), " ", $str);
	}
}

if (!function_exists('getUserIP')){
	function getUserIP(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			return $_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			return $_SERVER['REMOTE_ADDR'];
		}
	}
}

if (!function_exists('getArrayPermissions')){
	function getArrayPermissions($array){
		$arrRetorno = false;
		if (is_array($array)){
			foreach ($array as $key => $value){
				$arrRetorno[$value['FRM_ID']][$value['ACO_Descricao']] = $value['ACO_Descricao'];
			}
		}
		return $arrRetorno;
	}
}

if (!function_exists('checkPermission')){
	function checkPermission($acao, $array){
		if (is_array($array)){
			if (in_array($acao, $array)) return true;
		}
		return false;
	}
}

if (!function_exists('checkSession')){
	function checkSession($object){
		if (!$object->session->userdata('session_USU_ID')){
			redirect($object->router->routes['default_controller']);
		}

		if (!$object->session->userdata('session_filtro_grupos_empresas')){
			if ($object->session->userdata('session_TIP_ID') == 1){
				$object->session->set_userdata('session_filtro_grupos_empresas', '');
			}else{
				$object->session->set_userdata('session_filtro_grupos_empresas', array('GRE_ID' => $object->session->userdata('session_GRE_ID')));
			}
		}
		return true;
	}
}

if (!function_exists('checkNewForm')){
	function checkNewForm($method, $object){
		if ($arrFormularios = $object->FormulariosDAO->GetAll(array('FRM_Status' => SGP_ATIVO, 'FRM_Caminho' => $method))){
			$formularioID = $arrFormularios[0]['FRM_ID'];
			if (checkPermission(SGP_ACAO_SALVAR, $object->session->userdata('session_GetPerfilPermissoes')[$formularioID])){
				if ($object->LogDAO->LogInsert($object->session->userdata('session_USU_ID'), $formularioID, $method)){
					return $arrFormularios[0];
				}else{
					$object->session->set_userdata('error_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');
					send_email(SGP_MAIL_TO, SGP_SUBJECT_ERROR_QUERY_LOG_NAVIGATOR, $object->session->userdata('error_crud'));

					$object->load->view('home');
				}
			}else{
				$object->session->set_userdata('danger_crud', SGP_ACCESS_DENIED.' ('.$method.').');

				redirect($object->router->routes['home']);
			}
		}else{
			$object->session->set_userdata('danger_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');

			redirect($object->router->routes['home']);
		}
	}
}

if (!function_exists('checkNewView')){
	function checkNewView($method, $object){
		if ($arrFormularios = $object->FormulariosDAO->GetAll(array('FRM_Status' => SGP_ATIVO, 'FRM_Caminho' => $method))){
			$formularioID = $arrFormularios[0]['FRM_ID'];
			if (checkPermission(SGP_ACAO_CONSULTAR, $object->session->userdata('session_GetPerfilPermissoes')[$formularioID])){
				if ($object->LogDAO->LogInsert($object->session->userdata('session_USU_ID'), $formularioID, $method)){
					return $arrFormularios[0];
				}else{
					$object->session->set_userdata('error_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');
					send_email(SGP_MAIL_TO, SGP_SUBJECT_ERROR_QUERY_LOG_NAVIGATOR, $object->session->userdata('error_crud'));

					$object->load->view('home');
				}
			}else{
				$object->session->set_userdata('danger_crud', SGP_ACCESS_DENIED.' ('.$method.').');

				redirect($object->router->routes['home']);
			}
		}else{
			$object->session->set_userdata('danger_crud', SGP_ERRO_PROCESSAMENTO.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($object->router->routes['home']);
		}
	}
}

if (!function_exists('checkEditForm')){
	function checkEditForm($method, $object){
		if ($arrFormularios = $object->FormulariosDAO->GetAll(array('FRM_Status' => SGP_ATIVO, 'FRM_Caminho' => $method))){
			$formularioID = $arrFormularios[0]['FRM_ID'];
			if (checkPermission(SGP_ACAO_EDITAR, $object->session->userdata('session_GetPerfilPermissoes')[$formularioID])){
				if ($object->LogDAO->LogInsert($object->session->userdata('session_USU_ID'), $formularioID, $method)){
					return $arrFormularios[0];
				}else{
					$object->session->set_userdata('error_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');
					send_email(SGP_MAIL_TO, SGP_SUBJECT_ERROR_QUERY_LOG_NAVIGATOR, $object->session->userdata('error_crud'));
					$object->load->view('home');
				}
			}else{
				$object->session->set_userdata('danger_crud', SGP_ACCESS_DENIED.' ('.$method.').');
				redirect($object->router->routes['home']);
			}
		}else{
			$object->session->set_userdata('danger_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');
			redirect($object->router->routes['home']);
		}
	}
}

if (!function_exists('checkSaveForm')){
	function checkSaveForm($formID, $method, $object){
		if ($object->LogDAO->LogInsert($object->session->userdata('session_USU_ID'), base64_decode($formID), $method)){
			return true;
		}else{
			$object->session->set_userdata('danger_crud', SGP_ERRO_PROCESSAMENTO.' ('.$method.').');
			redirect($object->router->routes['home']);
		}
	}
}

if (!function_exists('checkDeleteForm')){
	function checkDeleteForm($formID, $method, $object){
		if ($object->LogDAO->LogInsert($object->session->userdata('session_USU_ID'), base64_decode($formID), $method)){
			return true;
		}
		return false;
	}
}
