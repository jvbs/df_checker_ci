 <?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter String Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/string_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('trim_slashes'))
{
	/**
	 * Trim Slashes
	 *
	 * Removes any leading/trailing slashes from a string:
	 *
	 * /this/that/theother/
	 *
	 * becomes:
	 *
	 * this/that/theother
	 *
	 * @todo	Remove in version 3.1+.
	 * @deprecated	3.0.0	This is just an alias for PHP's native trim()
	 *
	 * @param	string
	 * @return	string
	 */
	function trim_slashes($str)
	{
		return trim($str, '/');
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('strip_slashes'))
{
	/**
	 * Strip Slashes
	 *
	 * Removes slashes contained in a string or in an array
	 *
	 * @param	mixed	string or array
	 * @return	mixed	string or array
	 */
	function strip_slashes($str)
	{
		if ( ! is_array($str))
		{
			return stripslashes($str);
		}

		foreach ($str as $key => $val)
		{
			$str[$key] = strip_slashes($val);
		}

		return $str;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('strip_quotes'))
{
	/**
	 * Strip Quotes
	 *
	 * Removes single and double quotes from a string
	 *
	 * @param	string
	 * @return	string
	 */
	function strip_quotes($str)
	{
		return str_replace(array('"', "'"), '', $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('quotes_to_entities'))
{
	/**
	 * Quotes to Entities
	 *
	 * Converts single and double quotes to entities
	 *
	 * @param	string
	 * @return	string
	 */
	function quotes_to_entities($str)
	{
		return str_replace(array("\'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('reduce_double_slashes'))
{
	/**
	 * Reduce Double Slashes
	 *
	 * Converts double slashes in a string to a single slash,
	 * except those found in http://
	 *
	 * http://www.some-site.com//index.php
	 *
	 * becomes:
	 *
	 * http://www.some-site.com/index.php
	 *
	 * @param	string
	 * @return	string
	 */
	function reduce_double_slashes($str)
	{
		return preg_replace('#(^|[^:])//+#', '\\1/', $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('reduce_multiples'))
{
	/**
	 * Reduce Multiples
	 *
	 * Reduces multiple instances of a particular character.  Example:
	 *
	 * Fred, Bill,, Joe, Jimmy
	 *
	 * becomes:
	 *
	 * Fred, Bill, Joe, Jimmy
	 *
	 * @param	string
	 * @param	string	the character you wish to reduce
	 * @param	bool	TRUE/FALSE - whether to trim the character from the beginning/end
	 * @return	string
	 */
	function reduce_multiples($str, $character = ',', $trim = FALSE)
	{
		$str = preg_replace('#'.preg_quote($character, '#').'{2,}#', $character, $str);
		return ($trim === TRUE) ? trim($str, $character) : $str;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('random_string'))
{
	/**
	 * Create a Random String
	 *
	 * Useful for generating passwords or hashes.
	 *
	 * @param	string	type of random string.  basic, alpha, alnum, numeric, nozero, unique, md5, encrypt and sha1
	 * @param	int	number of characters
	 * @return	string
	 */
	function random_string($type = 'alnum', $len = 8)
	{
		switch ($type)
		{
			case 'basic':
				return mt_rand();
			case 'alnum':
			case 'numeric':
			case 'nozero':
			case 'alpha':
				switch ($type)
				{
					case 'alpha':
						$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'alnum':
						$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'numeric':
						$pool = '0123456789';
						break;
					case 'nozero':
						$pool = '123456789';
						break;
				}
				return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
			case 'unique': // todo: remove in 3.1+
			case 'md5':
				return md5(uniqid(mt_rand()));
			case 'encrypt': // todo: remove in 3.1+
			case 'sha1':
				return sha1(uniqid(mt_rand(), TRUE));
		}
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('increment_string'))
{
	/**
	 * Add's _1 to a string or increment the ending number to allow _2, _3, etc
	 *
	 * @param	string	required
	 * @param	string	What should the duplicate number be appended with
	 * @param	string	Which number should be used for the first dupe increment
	 * @return	string
	 */
	function increment_string($str, $separator = '_', $first = 1)
	{
		preg_match('/(.+)'.preg_quote($separator, '/').'([0-9]+)$/', $str, $match);
		return isset($match[2]) ? $match[1].$separator.($match[2] + 1) : $str.$separator.$first;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('alternator'))
{
	/**
	 * Alternator
	 *
	 * Allows strings to be alternated. See docs...
	 *
	 * @param	string (as many parameters as needed)
	 * @return	string
	 */
	function alternator()
	{
		static $i;

		if (func_num_args() === 0)
		{
			$i = 0;
			return '';
		}

		$args = func_get_args();
		return $args[($i++ % count($args))];
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('repeater'))
{
	/**
	 * Repeater function
	 *
	 * @todo	Remove in version 3.1+.
	 * @deprecated	3.0.0	This is just an alias for PHP's native str_repeat()
	 *
	 * @param	string	$data	String to repeat
	 * @param	int	$num	Number of repeats
	 * @return	string
	 */
	function repeater($data, $num = 1)
	{
		return ($num > 0) ? str_repeat($data, $num) : '';
	}
}

//Imprimir todo o array
if (!function_exists('pa')){
	function pa($array){
		echo '<pre>'; print_r($array);
	}
}

//Imprimir todas as constantes PHP
if (!function_exists('pc')){
	function pc(){
		echo '<pre>'; print_r(get_defined_constants(true));
	}
}

if (!function_exists('exibirStatus')){
	function exibirStatus($valor){
		$retorno = null;
		if ($valor == 'A'){
			$retorno = "<img style='text-align:center;' src='".base_url('assets/images/icone-ativo.png')."' border='0' title='Registro ativo'>";
		}elseif ($valor == 'I'){
			$retorno = "<img style='text-align:center;' src='".base_url('assets/images/icone-inativo.png')."' border='0' title='Registro inativo'>";

		}elseif ($valor == 'F'){
			$retorno = "Física";
		}elseif ($valor == 'J'){
			$retorno = "Jurídica";
		}
		return $retorno;
	}
}

if (!function_exists('formatarCPFouCNPJ')){
	function formatarCPFouCNPJ($strCampo, $booFormatado = true){
		//retira formato
		$strCodigoLimpo = @ereg_replace("[' '-./ t]", '', $strCampo);

		// pega o tamanho da string menos os digitos verificadores
		$intTamanho = (strlen($strCodigoLimpo) - 2);

		//verifica se o tamanho do código informado é válido
		if ($intTamanho != 9 && $intTamanho != 12){
			return "00.000.000/0000-00";
		}

		if ($booFormatado){
			// seleciona a máscara para cpf ou cnpj
			$strMascara = ($intTamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';
			$intIndice = -1;

			for ($intI=0; $intI < strlen($strMascara); $intI++){
				if ($strMascara[$intI] == '#') $strMascara[$intI] = $strCodigoLimpo[++$intIndice];
			}

			//retorna o campo formatado
			$strRetorno = $strMascara;
		}else{
			//se não quer formatado, retorna o campo limpo
			$strRetorno = $strCodigoLimpo;
		}

		return $strRetorno;
	}
}

if (!function_exists('removerPontuacoes')){
	function removerPontuacoes($strTexto){
		$strTexto = str_replace(".", "", $strTexto);
		$strTexto = str_replace(",", "", $strTexto);
		$strTexto = str_replace("-", "", $strTexto);
		$strTexto = str_replace("/", "", $strTexto);
		$strTexto = str_replace("(", "", $strTexto);
		$strTexto = str_replace("`", "", $strTexto);
		$strTexto = str_replace("'", "", $strTexto);
		$strTexto = str_replace("[", "", $strTexto);
		$strTexto = str_replace("]", "", $strTexto);
		return str_replace(")", "", $strTexto);
	}
}

if (!function_exists('limpaArray')){
	function limpaArray($array){
		if (is_array($array)){
			$arrRetorno = null;
			foreach($array as $chave => $valor){
				$arrRetorno[removerPontuacoes($chave)] = removerPontuacoes2($valor);
			}
			return $arrRetorno;
		}

		return false;
	}
}

if (!function_exists('notInsertArray')){
	function notInsertArray($value){
		if (!empty($value) || $value == '0'){
			$arrDadosNotAdd = array('(NULL)', '');
			if (!in_array($value, $arrDadosNotAdd)){
				return true;
			}
		}
		return false;
	}
}

if (!function_exists('ValidarCPFCNPJ')){
	function ValidarCPFCNPJ($strCPFCNPJ){
		$strCPFCNPJ = removerPontuacoes($strCPFCNPJ); // Elimina possivel mascara
		if (strlen($strCPFCNPJ) == 11){ //verifica se é CPF
			for ($t = 9; $t < 11; $t++){
				for ($d = 0, $c = 0; $c < $t; $c++){
					$d+= $strCPFCNPJ{$c} * (($t + 1) - $c);
				}

				$d = ((10 * $d) % 11) % 10;
				if ($strCPFCNPJ{$c} != $d) return false;
			}

			return $strCPFCNPJ;

		}elseif (strlen($strCPFCNPJ) == 14){ //verifica se é CNPJ
			$strCPFCNPJ = preg_replace ("@[./-]@", "", $strCPFCNPJ);
			if (strlen ($strCPFCNPJ) <> 14 or !is_numeric ($strCPFCNPJ)) return 0;

			$j     = 5;
			$k     = 6;
			$soma1 = "";
			$soma2 = "";

			for ($i = 0; $i < 13; $i++){
				$j      = $j == 1 ? 9 : $j;
				$k      = $k == 1 ? 9 : $k;
				$soma2 += ($strCPFCNPJ{$i} * $k);

				if ($i < 12){
					$soma1 += ($strCPFCNPJ{$i} * $j);
				}
				$k--;
				$j--;
			}

			$digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
			$digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

			if ( (($strCPFCNPJ{12} == $digito1) and ($strCPFCNPJ{13} == $digito2)) ) return $strCPFCNPJ;
		}else{
			return false;
		}
	}
}

if (!function_exists('checarDispositivo')){
	function checarDispositivo(){
		$retorno = false;
		$iphone  = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
		$ipad    = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
		$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
		$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
		$berry   = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
		$ipod    = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
		$symbian = strpos($_SERVER['HTTP_USER_AGENT'], "Symbian");

		if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true){
			$retorno = true;
		}

		return $retorno;
	}
}

if (!function_exists('removeStrings')){
	function removeStrings($string, $remove){
		$tam    = strlen($remove);
		$spaces = "";
		for ($i=0; $i < $tam; $i++) {
			$spaces.= " ";
		}
		return $string = str_replace(" ","",strtr($string,$remove,$spaces));
	}
}

if (!function_exists('clearBrowserCache')){
	function clearBrowserCache(){
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	}
}

if (!function_exists('showSizeString')){
	function showSizeString($string){
		return substr($string, 0, SGP_MAXSIZESTRING)."...";
	}
}

if (!function_exists('getFilterGroupCompany')){
	function getFilterGroupCompany($typeCompanyID = null){
		if (isset($typeCompanyID)){
			if ($typeCompanyID == 1) return true;
		}
		return false;
	}
}

if (!function_exists('getFiltroPerfis')){
	function getFiltroPerfis($objeto){
		$arrPerfis = null;
		if ($objeto->session->userdata('session_TIP_ID') == 1){
			$arrPerfis = $objeto->PerfisDAO->GetAll(array('PER_Status' => SGP_ATIVO));
		}elseif ($objeto->session->userdata('session_TIP_ID') == 2){
			$arrPerfis = $objeto->PerfisDAO->GetAll(
				array(
					'PER_Status' => SGP_ATIVO,
					'GRE_ID'     => $objeto->session->userdata('session_GRE_ID')
				),
				null,
				null,
				array('TIP_ID' => unserialize(SGP_PERFIS_NAO_ADMIN))
			);
		}else{
			$arrPerfis = $objeto->PerfisDAO->GetAll(
				array(
					'GRE_ID'     => $objeto->session->userdata('session_GRE_ID'),
					'TIP_ID'     => $objeto->session->userdata('session_TIP_ID'),
					'PER_Status' => SGP_ATIVO
				)
			);
		}

		$objeto->session->set_userdata('session_PER_filtros', $arrPerfis);
		return $objeto->session->userdata('session_PER_filtros');
	}
}

if (!function_exists('getFiltroGruposEmpresas')){
	function getFiltroGruposEmpresas($objeto){
		$arrGruposEmpresas = $objeto->GruposEmpresasDAO->GetAll(array('GRE_Status' => SGP_ATIVO));
		$objeto->session->set_userdata('session_GRE_filtros', $arrGruposEmpresas);

		return $objeto->session->userdata('session_GRE_filtros');
	}
}

if (!function_exists('getFiltroUsuarios')){
	function getFiltroUsuarios($objeto){
		$arrFiltros['USU_Status'] = SGP_ATIVO;
		if ($objeto->session->userdata('session_TIP_ID') == 1){

		}elseif ($objeto->session->userdata('session_TIP_ID') == 1){
			$arrFiltros['GRE_ID']     = $objeto->session->userdata('session_GRE_ID');
		}else{
			$arrFiltros['USU_ID']     = $objeto->session->userdata('session_USU_ID');
		}


		$objeto->session->set_userdata('session_USU_filtros', $arrFiltros);
		return $objeto->session->userdata('session_USU_filtros');
	}
}

if(!function_exists('UserHasPermissions')){
    function UserHasPermissions($objeto ,$permission){
        $arrayPermissions = explode(',', $objeto->session->userdata('session_USU_Permissoes'));
        if(in_array($permission, $arrayPermissions)){
            return true;
        } else {
            return false;
        }
    }
}

if(!function_exists('generateCustomKey')){
    function generateCustomKey(){
        return substr(date('YmdHi').rand(),0,15);
    }
}


if (!function_exists('removeAccents')){
	function removeAccents($string) {
		return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
	}
}
