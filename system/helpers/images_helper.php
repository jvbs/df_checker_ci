<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('upload_image')){
	function upload_image($image, $path = null){
		$arrPermitidos = array("image/pjpeg", "image/x-png", "image/jpeg", "image/png", "image/gif");		
		if (is_array($image) && !empty($path)){
			if (in_array($image["type"], $arrPermitidos)){
				if (is_uploaded_file($image['tmp_name'])){					
					if (move_uploaded_file($image['tmp_name'], $path.$image['name'])){
						$arrExp = explode('.', $image['name']);
						if (count($arrExp) > 0){
							$concat     = date('YmdHis').rand(1000, 100000000000000).".".$arrExp[count($arrExp)-1];
							$strArquivo = $path.$concat;
							if (rename($path.$image['name'], $strArquivo)){
								return $concat;
							}else{
								return $image['name'];
							}
						}
					}
				}
			}
		}
		return '(NULL)';
	}
}