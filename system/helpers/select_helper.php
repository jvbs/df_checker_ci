<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('select_user_level')){
	function select_user_level($obrigatorio = CI_SELECT_OBRIGATORIO, $selecionado = null){
		$strHtml = "<select id='user_level' name='user_level' class='form-control' ".$obrigatorio.">";
			$strHtml.= "<option value=''>".CI_SELECIONE."</option>";

				$strSelected1 = null;
				$strSelected2 = null;
				$strSelected3 = null;

				if (!empty($selecionado)){
					if ($selecionado == CI_VALUE_USER_NIVEL1){
						$strSelected1 = CI_SELECT_SELECTED;
					}elseif ($selecionado == CI_VALUE_USER_NIVEL2){
						$strSelected2 = CI_SELECT_SELECTED;
					}elseif ($selecionado == CI_VALUE_USER_NIVEL2){
						$strSelected3 = CI_SELECT_SELECTED;
					}
				}

				$strHtml.= "<option ".$strSelected1." value='".base64_encode(CI_VALUE_USER_NIVEL1)."'>".CI_LABEL_USER_NIVEL1."</option>";
				$strHtml.= "<option ".$strSelected2." value='".base64_encode(CI_VALUE_USER_NIVEL2)."'>".CI_LABEL_USER_NIVEL2."</option>";
				$strHtml.= "<option ".$strSelected3." value='".base64_encode(CI_VALUE_USER_NIVEL3)."'>".CI_LABEL_USER_NIVEL3."</option>";
		$strHtml.= "</select>";
		return $strHtml;
	}
}

if (!function_exists('select_clients')){
	function select_clients($arrDados, $obrigatorio = CI_SELECT_OBRIGATORIO, $selecionado = null){
		$strHtml = "<select id='client_id' name='client_id' class='form-control' ".$obrigatorio.">";
			$strHtml.= "<option value=''>".CI_SELECIONE."</option>";

				if ($arrDados != null){
					foreach($arrDados as $l){
						$selected = null;
						if ($selecionado == $l['id']) $selected = CI_SELECT_SELECTED;

						$strHtml.= "<option ".$selected." value='".$l['id']."'>".$l['fantasy_name']."</option>";
					}
				}

		$strHtml.= "</select>";
		return $strHtml;
	}
}

if (!function_exists('select_analyst')){
	function select_analyst($arrDados, $obrigatorio = CI_SELECT_OBRIGATORIO, $selecionado = null){
		$strHtml = "<select id='analyst_id' name='analyst_id' class='form-control' ".$obrigatorio.">";
			$strHtml.= "<option value=''>".CI_SELECIONE."</option>";
				if ($arrDados != null){
					foreach($arrDados as $l){
						$selected = null;
						if ($selecionado == $l['id']) $selected = CI_SELECT_SELECTED;

						$strHtml.= "<option ".$selected." value='".$l['id']."'>".$l['name']."</option>";
					}
				}

		$strHtml.= "</select>";
		return $strHtml;
	}
}

if (!function_exists('select_report')){
	function select_report($arrDados, $obrigatorio = CI_SELECT_OBRIGATORIO, $selecionado = null){
		$strHtml = "<select id='report_id' name='report_id[]' class='form-control' ".$obrigatorio.">";
			$strHtml.= "<option value=''>".CI_SELECIONE."</option>";
				if ($arrDados != null){
					foreach($arrDados as $l){
						$selected = null;
						if ($selecionado == $l['id']) $selected = CI_SELECT_SELECTED;

						$strHtml.= "<option ".$selected." value='".$l['id']."'>".$l['report_name']."</option>";
					}
				}

		$strHtml.= "</select>";
		return $strHtml;
	}
}

if (!function_exists('select_querylist')){
	function select_querylist($arrDados, $obrigatorio = CI_SELECT_OBRIGATORIO, $selecionado = null){
		$strHtml = "<select id='query_list_id' name='query_list_id' class='form-control' ".$obrigatorio.">";
			$strHtml.= "<option value=''>".CI_SELECIONE."</option>";

				if ($arrDados != null){
					foreach($arrDados as $l){
						$strHtml.= "<option value='".$l['id']."'>".$l['table_type']." (".$l['query_source'].")</option>";
					}
				}

		$strHtml.= "</select>";
		return $strHtml;
	}
}
