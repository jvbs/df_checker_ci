jQuery.extend(jQuery.validator.messages, {
    required: "Obrigatório.",
    remote: "Please fix this field.",
    email: "E-mail inválido.",
    url: "Please enter a valid URL.",
    date: "Entre com uma data válida",
    dateISO: "Please enter a valid date (ISO).",
    number: "Digite um número válido.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Não exceda {0} caracters."),
    minlength: jQuery.validator.format("Mínimo de {0} caracters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});