$(document).ready(function (){
    $('[data-toggle="tooltip"]').tooltip({html:true});
    $('[rel="txtTooltip"]').tooltip({html:true});

	$(".maskMoney").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});

	$('#div-success-delete').hide();

	//Tempo Fade Out
	$('#div-danger-home, #div-success-home, div-success-delete, #div-success-view, #div-danger-view, #div-warning-view, #div-warning-home').fadeOut(parseInt($('#hddFadeOut').val()));

	//Redirecionamento botão cancelar
	$("#btnCancelar").click(function (e){
		redir($.trim($('#hddHome').val()), 'parent');
	});

	//Date picker
	$('.hasDatepicker').datepicker({ autoclose: true, format: 'dd/mm/yyyy', language: 'pt-BR' }).on('changeDate',
	function (ev){
		(ev.viewMode == 'days') ? $(this).datepicker('hide') : '';
	});

	//Validando campos formulário de usuários;
	$('#email, #password').keydown(function(e){
		if (e.keyCode == 32) return false;
	});

	//Itens da ordem de serviço
	$('#btnItens').click(function(e){
		$.post($.trim($('#hddServiceDetailNews').val()), { id: $.trim($('#id').val()) },
		function(data){
			//alert(data);
			if(data.sucesso == 'true'){
				dialogAlert("Informação", data.strHtml, 2, '');

				setTimeout(function(){
				  carregarItensOrdem();
				}, 1000);
			}
		  }, 'json'
		);
	});

	$('#email').keyup(function(){
		//if (this.value.length >= 5){
		$.post($.trim($('#hddConsultarDados').val()), { USU_ID: $.trim($('#id').val()), USU_Email: this.value },
		function(data){
			//alert(data);
			if(data.sucesso == 'true'){
				$('#lbl-mail').removeClass("has-error").addClass("has-success");
				$('#grp-mail').removeClass("has-error").addClass("has-success");
				$('#grp-mail2').removeClass("has-error").addClass("has-success");
				$('#spn-mail').removeClass("has-error").addClass("has-success");
			}else{
				$('#lbl-mail').removeClass("has-success ").addClass("has-error");
				$('#grp-mail').removeClass("has-success").addClass("has-error");
				$('#grp-mail2').removeClass("has-success").addClass("has-error");
				$('#spn-mail').removeClass("has-success").addClass("has-error");
			}
			validacaoExibirBtnSalvarUsuarios();
		  }, 'json'
		);
		//}
	});

	//Apenas números input css
	$(".numericOnly").keypress(function (e){
		if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
	});

	$('.noTrim').keypress(function(e){
		if(e.which === 32) return false;
	});

    $('.maskCPF').mask('999.999.999-99', {reverse: true});	
    $('.maskCNPJ').mask('99.999.999/9999-99', {reverse: true});
	$('.maskTelefone').mask('(99)9999-9999', {reverse: true});
	$('.maskCelular').mask('(99)99999-9999', {reverse: true});
	$('.maskCEP').mask("99999-999");

	//Chamadas de funções inicial
	carregarFormularios();
});
