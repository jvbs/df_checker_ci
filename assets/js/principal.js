/* codificação utf-8 */
function mLibCarregaTela(div, tela, formularioID, strAcao){
  jQuery("#hddAcao").val(strAcao);
  jQuery("#"+div).html("");
  jQuery("#"+div).html("<font class='fontTextBranca'>Carregando...</font>");
  jQuery("#"+div).load(tela);
  //hddAcao
  //logNavegacao(formularioID, jQuery.trim(jQuery("#hddAcao").val()));
}

function logNavegacao(formularioID, strAcaoLog){
  jQuery.post('../../../modulos/sistema/gerencial/controladores/NavegacoesControlador.php', { 
    strAcao : 'registroNavegacoes',
    strAcaoLog: strAcaoLog,
    FRM_ID : formularioID
  },
    function(data){
      //console.log(data); return;
      if(data.sucesso == 'true'){

      }else{
        jQuery('#dialog-atencao').html(data.mensagem);
        jQuery('#dialog-atencao').dialog('open');
      }          	
    }, 'json'    
  );
}

function preLoadingOpen(){
    jQuery.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
}
    
function preLoadingClose(){
    jQuery.unblockUI(); 
}