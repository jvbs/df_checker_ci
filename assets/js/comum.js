var strCarregando    = $.trim($('#hddCarregando').val());
var strCarregandoCor = $.trim($('#hddCarregandoCor').val());
var strSelecione     = $.trim($('#hddSelecione').val());
var strAtencao       = $.trim($('#hddInfoError').val());
var strInformacao    = $.trim($('#hddInfoAlert').val());
var strHtml          = '';

function preLoadingOpen(){
    $.blockUI({ css: {
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } });
}

function preLoadingClose(){
    $.unblockUI();
}

function carregarCidades(estadoID, cidadeSelecionada){
	strHtml = "<option value=''>"+strSelecione+"</option>";
	$.post($.trim($('#hddPesquisarCidades').val()),
	{
		UF_ID: estadoID,
		FRM_ID2: $.trim($('#FRM_ID2').val())
	},
	function(data){
		//alert(data); return;
		if(data.sucesso == 'true'){
			if (data.arrDados.length > 0){
				var strSelected = '';
				for (var i=0; i<data.arrDados.length; i++){
					strSelected = '';
					if (data.arrDados[i].CID_ID == cidadeSelecionada){
						strSelected = 'selected';
					}
					strHtml+= "<option "+strSelected+" value='"+data.arrDados[i].CID_ID+"'>"+data.arrDados[i].CID_Descricao+"</option>";
		        }
				$('#CID_ID').html(strHtml);
			}
		}
	  }, 'json'
	);
}

function carregarBairros(cidadeID, bairroSelecionado){
	strHtml = "<option value=''>"+strSelecione+"</option>";
	$.post($.trim($('#hddPesquisarBairros').val()), 
	{
		CID_ID: cidadeID,
		FRM_ID2: $.trim($('#FRM_ID2').val())
	},
	function(data){
		//alert(data); return;
		if(data.sucesso == 'true'){							
			if (data.arrDados.length > 0){
				var strSelected = '';
				for (var i=0; i<data.arrDados.length; i++){
					strSelected = '';
					if (data.arrDados[i].BAI_ID == bairroSelecionado){
						strSelected = 'selected';
					}
					strHtml+= "<option "+strSelected+" value='"+data.arrDados[i].BAI_ID+"'>"+data.arrDados[i].BAI_Descricao+"</option>";
		        }
				$('#BAI_ID').html(strHtml);
			}
		}
	  }, 'json'
	);	
}

function validacaoExibirBtnSalvarUsuarios(){
	//Verifica se é modo de INCLUSÃO ou ATUALIZAÇÃO
	if ($.trim($('#id').val()) == ''){
		if ( $('#grp-mail').attr('class').includes('has-success') && $('#grp-pass').attr('class').includes('has-success') && $('#grp-pass2').attr('class').includes('has-success') ){
			$('#btnSalvar').prop('disabled', false);
		}else{
			$('#btnSalvar').prop('disabled', true);
		}
	}else{
		if ($('#grp-mail').attr('class').includes('has-success')){
			$('#btnSalvar').prop('disabled', false);
		}else{
			$('#btnSalvar').prop('disabled', true);
		}
	}
}

function checarSenhas(strSenha, strSenha2){
	if ($.trim(strSenha) != '' && $.trim(strSenha2) != ''){
		if (strSenha != strSenha2){
			$('#lbl-pass').removeClass("has-success").addClass("has-error");
			$('#grp-pass').removeClass("has-success").addClass("has-error");
			$('#lbl-pass2').removeClass("has-success").addClass("has-error");
			$('#grp-pass2').removeClass("has-success").addClass("has-error");

			dialogAlert("ATENÇÃO", "Senhas precisa ser iguais.", 5, '');
		}else{
			$('#lbl-pass').removeClass("has-error").addClass("has-success");
			$('#grp-pass').removeClass("has-error").addClass("has-success");		
			$('#lbl-pass2').removeClass("has-error").addClass("has-success");
			$('#grp-pass2').removeClass("has-error").addClass("has-success");		
		}
	}

	validacaoExibirBtnSalvarUsuarios();
}

function adicionarFormularioAcao(checkboxID, formularioID, acaoID){
	var strAcao = 'Excluir';
	if ($('#'+checkboxID).prop('checked')) strAcao = 'Adicionar';

	$.post($.trim($('#hddAdicionarRemoverFormularioAcao').val()),
	{
		ACO_ID: acaoID, 
		FRM_ID: formularioID,
		FRM_ID2: $.trim($('#FRM_ID2').val())
	},
	function(data){
		//alert(data); return;
		if(data.sucesso == 'true'){ }
	  }, 'json'
	);
}


function checkboxValida(checkboxID){
	if (typeof($('#'+checkboxID) != 'undefined')){
		if ($('#'+checkboxID).prop('checked')){
			$('#'+checkboxID).prop('checked', false);
		}else{
			$('#'+checkboxID).prop('checked', true);
		}
	}
}

function excluir(acao){
	$('#linkExcluir').html(strCarregando);

	if ( acao.includes('querylist_delete') || acao.includes('users_delete') || acao.includes('clients_delete') ||
	acao.includes('serviceorders_delete') || acao.includes('servicedetail_delete')
	){
		$.post(acao,
		function(data){
			//alert(data); return;
			if (data.sucesso == 'true'){
				$('#hddExcluir').val('');
				$('#descricaoExcluir').html('');
				$('#linkExcluir').html('Ok');
				$('.modal-backdrop').hide();
				
				if (acao.includes('servicedetail_delete')){
					carregarItensOrdem();
					return;
				}

				redir(data.redir, 'parent');
			}else{
				redir($('#hddHome').val(), 'parent');				
			}
		  }, 'json'
		);
	}else{
		redir($('#hddHome').val(), 'parent');				
	}
}

function carregarTela(caminho){
	redir(caminho);
}

function mLibMarcar(x){
  var strChk  = 'id'+x;
  var strCel  = 'linha'+x;  
  var strCor  = $('#'+strCel).css("background-color");
  var strCel2 = jQuery('#'+strCel);

  if ($('#'+strChk).is(':checked')){
    $('#'+strCel).css('background-color', '#FFF868');
  }else{
    $('#'+strCel).css('background-color', strCor);
  }
}

/* codificaÃ§Ã£o utf-8 */
function isDataValida(data){
    //alert(data);
    /******** VALIDA DATA NO FORMATO DD/MM/AAAA *******/
    var regExpCaracter = /[^\d]/;     //ExpressÃƒÂ£o regular para procurar caracter nÃƒÂ£o-numÃƒÂ©rico.
    var regExpEspaco = /^\s+|\s+$/g;  //ExpressÃƒÂ£o regular para retirar espaÃƒÂ§os em branco.

    if(data.length != 10){
        return false;
    }

    var splitData = data.split('/');
    if (splitData.length != 3){
        return false;
    }

    /* Retira os espaÃƒÂ§os em branco do inÃƒÂ­cio e fim de cada string. */
    splitData[0] = splitData[0].replace(regExpEspaco, '');
    splitData[1] = splitData[1].replace(regExpEspaco, '');
    splitData[2] = splitData[2].replace(regExpEspaco, '');

    if ((splitData[0].length != 2) || (splitData[1].length != 2) || (splitData[2].length != 4)){
        return false;
    }

    /* Procura por caracter nÃƒÂ£o-numÃƒÂ©rico. EX.: o "x" em "28/09/2x11" */
    if (regExpCaracter.test(splitData[0]) || regExpCaracter.test(splitData[1]) || regExpCaracter.test(splitData[2])){
        return false;
    }

    var dia = parseInt(splitData[0],10);
    var mes = parseInt(splitData[1],10)-1; //O JavaScript representa o mÃƒÂªs de 0 a 11 (0->janeiro, 1->fevereiro... 11->dezembro)
    var ano = parseInt(splitData[2],10);

    var novaData = new Date(ano, mes, dia);

    /* O JavaScript aceita criar datas com, por exemplo, mÃƒÂªs=14, porÃƒÂ©m a cada 12 meses mais um ano ÃƒÂ© acrescentado ÃƒÂ  data
    final e o restante representa o mÃƒÂªs. O mesmo ocorre para os dias, sendo maior que o nÃƒÂºmero de dias do mÃƒÂªs em
    questÃƒÂ£o o JavaScript o converterÃƒÂ¡ para meses/anos.
    Por exemplo, a data 28/14/2011 (que seria o comando "new Date(2011,13,28)", pois o mÃƒÂªs ÃƒÂ© representado de 0 a 11)
    o JavaScript converterÃƒÂ¡ para 28/02/2012.
    Dessa forma, se o dia, mÃƒÂªs ou ano da data resultante do comando "new Date()" for diferente do dia, mÃƒÂªs e ano da
    data que estÃƒÂ¡ sendo testada esta data ÃƒÂ© invÃƒÂ¡lida. */
    if ((novaData.getDate() != dia) || (novaData.getMonth() != mes) || (novaData.getFullYear() != ano)){
        return false;
    }else{
        return true;
    }
}

function isEmail(email){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

    if(typeof(email) == "string"){
        if(er.test(email)){ return true; }
    }else if(typeof(email) == "object"){
        if(er.test(email.value)){
            return true;
        }
    }

    return false;
}

function validarCPF(cpf){
    var erro = new String;
    cpf = cpf.replace( /[.-]/g, "" );

    if (cpf.length == 11){
        var nonNumbers = /\D/;

        if (nonNumbers.test(cpf))
        {
                return false;
        }
        else
        {
            if (cpf == "00000000000" ||
                    cpf == "11111111111" ||
                    cpf == "22222222222" ||
                    cpf == "33333333333" ||
                    cpf == "44444444444" ||
                    cpf == "55555555555" ||
                    cpf == "66666666666" ||
                    cpf == "77777777777" ||
                    cpf == "88888888888" ||
                    cpf == "99999999999") {

                    return false;
            }

            var a = [];
            var b = new Number;
            var c = 11;

            for (var i=0; i<11; i++){
                    a[i] = cpf.charAt(i);
                    if (i < 9) b += (a[i] * --c);
            }

            var x, y;

            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }

            b = 0;
            c = 11;

            for (y=0; y<10; y++) b += (a[y] * c--);

            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])) {
                    return false;
            }
        }
    }else{
        if(cpf.length == 0)
            return false
        else
            return false;
    }
    return true;
}

function validarCNPJ(cnpj){
    var result = true;

    // Limpa pontos e TraÃƒÆ’Ã‚Â§os da string
    cnpj = cnpj.replace(/\./g, "");
    cnpj = cnpj.replace(/\-/g, "");
    cnpj = cnpj.replace(/\_/g, "");
    cnpj = cnpj.replace(/\//g, "");

    if(jQuery.trim(cnpj) != ""){
        if(cnpj.length!=14){ result = false; }

        var pri = eval(cnpj.substring(0,2));
        var seg = eval(cnpj.substring(3,6));
        var ter = eval(cnpj.substring(7,10));
        var qua = eval(cnpj.substring(11,15));
        var qui = eval(cnpj.substring(16,18));

        var i;
        var numero;

        numero = (pri+seg+ter+qua+qui);

        var s = numero;

        var c = cnpj.substr(0,12);

        var dv = cnpj.substr(12,2);
        var d1 = 0;

        for (i = 0; i < 12; i++){
            d1 += c.charAt(11-i)*(2+(i % 8));
        }

        if (d1 == 0){
            result = false;
        }

        d1 = 11 - (d1 % 11);

        if (d1 > 9) d1 = 0;

        if (dv.charAt(0) != d1){
            result = false;
        }

        d1 *= 2;

        for (i = 0; i < 12; i++){
            d1 += c.charAt(11-i)*(2+((i+1) % 8));
        }

        d1 = 11 - (d1 % 11);

        if (d1 > 9) d1 = 0;

        if (dv.charAt(1) != d1){
            result = false;
        }
    }
    if(!result){
        return false;
    }
    return true;
}

function isHoraValida(valor){
    var strHora = valor.split(":");

    if (parseInt(strHora[0]) > parseInt(23)){
        return false;
    }else if (parseInt(strHora[1]) > parseInt(59)){
        return false;
    }else{
        return true;
    }
}

function cliqueEnter(e, funcao){
    if (e.keyCode == 13){
        //aÃ§Ã£o quando clicar na tecla ENTER;
		eval(funcao);
    }	
}

function formatarMoeda(valor, casas, separdor_decimal, separador_milhar){
    var valor_total = parseInt(valor * (Math.pow(10,casas)));
    var inteiros    =  parseInt(parseInt(valor * (Math.pow(10,casas))) / parseFloat(Math.pow(10,casas)));
    var centavos    = parseInt(parseInt(valor * (Math.pow(10,casas))) % parseFloat(Math.pow(10,casas)));

    if(centavos%10 == 0 && centavos+"".length<2 ){
        centavos = centavos+"0";
    }else if(centavos<10){
        centavos = "0"+centavos;
    }

    var milhares = parseInt(inteiros/1000);
    inteiros     = inteiros % 1000;
    var retorno  = "";

    if(milhares>0){
		retorno = milhares+""+separador_milhar+""+retorno;
		if(inteiros == 0){
			inteiros = "000";
		} else if(inteiros < 10){
			inteiros = "00"+inteiros;
		} else if(inteiros < 100){
			inteiros = "0"+inteiros;
		}
    }
    retorno += inteiros+""+separdor_decimal+""+centavos;

    return retorno;
}

function desabilitarBotao(strBotao){    
    $('#'+strBotao).attr('disabled', true);
    $('#'+strBotao).val('Carregando...');
    $('#'+strBotao).css('color', '#9b978f');
}

function habilitarBotao(strBotao, strValorBotao){
    if (strValorBotao == "") strValorBotao = "Salvar";
    $('#'+strBotao).attr('disabled', false);
    $('#'+strBotao).val(strValorBotao);
    $('#'+strBotao).css({'color':'#FFFFFF'});    
}

function redir(url, tipo){
	if (tipo == 'parent'){
		parent.location = url;
	}else{
		document.location.href = url;	
	}  
}

function dialogAlert(strTitulo, strMensagem, strTipo, strRedir){
  var types = [BootstrapDialog.TYPE_DEFAULT];    
  if (strTipo == 2){
    types = [BootstrapDialog.TYPE_INFO];
  }else if (strTipo == 3){
    types = [BootstrapDialog.TYPE_PRIMARY];
  }else if (strTipo == 4){
    types = [BootstrapDialog.TYPE_SUCCESS];
  }else if (strTipo == 5){
    types = [BootstrapDialog.TYPE_WARNING];
  }else if (strTipo == 6){
    types = [BootstrapDialog.TYPE_DANGER];
  }
  
  $.each(types, function(index, type){
    BootstrapDialog.show({
    type: type,
    title: $.trim(strTitulo),
    message: $.trim(strMensagem),
    buttons: [{
      label: 'Ok',
      action: function(dialogItself){
        dialogItself.close();
		if ($.trim(strRedir) != '') redir(strRedir);
      }
    }]});     
  });
}

function soNumeros(e,args){         
    // Funcao que permite apenas teclas numï¿½ricas e  
    // todos os caracteres que estiverem na lista 
    // de argumentos. 
    // Deve ser chamada no evento onKeyPress desta forma 
    //  onKeyPress="return (soNums(event,'0'));" 
    // caso queira apenas permitir caracters como por exemplo um campo que sï¿½ aceite valores em Hexadecimal (de 0 a F) usamos 
    //  onKeyPress ="return (soNums(event,'AaBbCcDdEeFf'));" 

/* Esta parte comentada ï¿½ a que testei exaustivamente e garanto que funciona em praticamente todos os browsers 
        var evt='';// devido a um warning gerado pelo Console de Javascript que "enxergava" uma redeclaracao de "evt" decidi declara-la uma vez e alterar ser valor posteriormente  

        if (document.all){evt=event.keyCode;} // caso seja IE 
        else{evt = e.charCode;}    // do contrario deve ser Mozilla 
O cï¿½digo a seguir teste apenas em FireFox e Internet Explorer 6 e funcionou perfeitamente. Caso vc tenha algum problema com esta funcao por favor entre em contato 
*/ 
        var evt = (e.keyCode?e.keyCode:e.charCode);
        var chr = String.fromCharCode(evt);    // pegando a tecla digitada;
        // Se o cï¿½digo for menor que 20 ï¿½ porque deve ser caracteres de controle 
        // ex.: <ENTER>, <TAB>, <BACKSPACE> portanto devemos permitir 
        // as teclas numï¿½ricas vao de 48 a 57 
        return (evt <20 || (evt >47 && evt<58) || (args.indexOf(chr)>-1 ) ); 
}

function marcardesmarcar(){
  $('.marcar').each(
    function(){
      if ($(".marcar").prop("checked")){
        $(this).attr("checked", false);
      }else{
        $(this).attr("checked", true);
      }
    }
  );
}

function proximo(e, campo){
  if (e.keyCode == 13){
    $('#'+campo).focus();
  }
}

function carregarFormularios(){
	$("#ulFormularios").html(strCarregandoCor);
	$.post($.trim($('#hddCarregarFormularios').val()),
	  function(data){
		//alert(data); return;
		if (data.sucesso == 'true'){
			$("#ulFormularios").html(data.strHtml);
		}
	  }, 'json'
	);
}

function checarUsuarios(){
	$('#USU_Login').trigger('keyup');
	$('#USU_Email').trigger('keyup');	
	
	validacaoExibirBtnSalvarUsuarios();
}

function carregarGraficosUsuarios(){
	$.post($.trim($('#hddCarregarGraficoUsuarios').val()), { USU_Ano: $.trim($('#hddAnoAtual').val()) },
	  function(data){
		//alert(data); return;
		if (data.sucesso == 'true'){
			Highcharts.chart('chart-user', {
				chart: {
					type: 'column'
				},
				title: {
					text: $.trim($('#hddLabelGraficoUsuarios').val())+' ('+$.trim($('#hddAnoAtual').val())+')',
				},
				xAxis: {
					categories: data.arrCategories,
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: $.trim($('#hddTituloPagina').val())
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">{point.key}/'+$.trim($('#hddAnoAtual').val())+'</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">Quantidade: </td>' +
						'<td style="padding:0"><b>{point.y}</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [{
					name: $.trim($('#hddSigla').val()),
					data: data.arrDatas
				}]
			});
		}
	  }, 'json'
	);	
}

function carregarItensOrdem(){
	$.post($.trim($('#hddServiceDetailLists').val()), { service_id: $.trim($('#service_id').val()) }, 
	  function(data){
		//alert(data); return;
		if (data.sucesso == 'true'){
			$("#resultadoDetalhe").html(data.strHtml);
		}
	  }, 'json'
	);
}

function adicionarItensOrdem(){
	if ($.trim($('#query_list_id').val()) == ''){
		dialogAlert("ATENÇÃO", "Query List precisa ser informado.", 5, '');
		return;
	}else{
		$.post($.trim($('#hddServiceDetailSave').val()), { 
			service_id: $.trim($('#service_id').val()),
			query_list_id: $.trim($('#query_list_id').val())
		},
		function(data){		
			//alert(data);
			if(data.sucesso == 'true'){
				$('#query_list_id').val('');
				carregarItensOrdem();
				//spnItensAdd
			}else{
				dialogAlert("ATENÇÃO", data.message, 5, '');				
			}
		  }, 'json'
		);
	}	
}

function removerItensOrdem(){
	$.post($.trim($('#hddServiceDetailSave').val()), { 
		service_id: $.trim($('#service_id').val()),
		query_list_id: $.trim($('#query_list_id').val())
	},
	function(data){		
		//alert(data);
		if(data.sucesso == 'true'){
			$('#query_list_id').val('');
			carregarItensOrdem();
			//spnItensAdd
		}else{
			dialogAlert("ATENÇÃO", data.message, 5, '');				
		}
	  }, 'json'
	);
}