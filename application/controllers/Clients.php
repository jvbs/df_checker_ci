<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller{
    public function __construct(){
        parent::__construct();
		checkSession($this);
    }

	public function News(){
		$arrDados['strPagina'] = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVO;
		$arrDados['checar']    = null;
		$this->load->view('frmClients', $arrDados);
	}
    

	public function Lists(){
		$arrDados['arrDados']  = $this->ClientsDAO->GetAll(array('status' => CI_ATIVO));
		$arrDados['strPagina'] = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_CONSULTAR;

		$this->load->view('cntClients', $arrDados);
	}

	public function Edit(){
		if ($arrDados['arrDados'] = $this->ClientsDAO->GetAll(array('id' => $this->uri->segment(3)))){
			$arrDados['arrDados'][0]['id'] = base64_encode($arrDados['arrDados'][0]['id']);
			$arrDados['strPagina'] 		   = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_EDITAR;
			$arrDados['checar']    		   = null;

			if ($arrDados['arrDados'][0]['status'] == CI_INATIVO){
				$arrDados['checar'] = CI_CHECKBOX_CHECKED;
			}

			$this->load->view('frmClients', $arrDados);
		}else{
			$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($this->router->routes['clients_lists']);
		}
	}

	public function Save(){
		$arrDados['company_id']          = antinjection(removerPontuacoes($_POST['company_id']));
		$arrDados['telephone']   		 = antinjection(removerPontuacoes($_POST['telephone']));
		$arrDados['celphone'] 			 = antinjection(removerPontuacoes($_POST['celphone']));
		$arrDados['company_name']     	 = antinjection($_POST['company_name']);
		$arrDados['fantasy_name']     	 = antinjection($_POST['fantasy_name']);
		$arrDados['address']  			 = antinjection($_POST['address']);
		$arrDados['address_number']  	 = antinjection($_POST['address_number']);
		$arrDados['complement']    		 = antinjection($_POST['complement']);
		$arrDados['general_information'] = antinjection($_POST['general_information']);
		$arrDados['status']         	 = CI_ATIVO;

		if (isset($_POST['status'])) $arrDados['status'] = CI_INATIVO;

		if (!empty($_POST['id'])){
			#ATUALIZAÇÃO
			$arrDados['updated_by'] = $this->session->userdata('session_USU_ID');
			$arrDados['updated_at'] = dataHoraAtual();

			if ($this->ClientsDAO->Update(array('id' => antinjection(base64_decode($_POST['id']))), $arrDados)){
				$this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['clients_news']);
				}else{
					redirect('Clients/'.base64_decode($_POST['id']));
				}
			}
		}else{
			#INCLUSÃO
			$arrDados['created_by'] = $this->session->userdata('session_USU_ID');
			$arrDados['created_at'] = dataHoraAtual();
			if ($id = $this->ClientsDAO->InsertId($arrDados)){
				$this->session->set_userdata('success_crud', CI_CADASTRO_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['clients_news']);
				}else{
					redirect('Clients/'.$id);
				}
			}
		}
	}

	public function Delete(){
        if($this->uri->segment(2) != null){
            $id                 = $this->uri->segment(2);
            $arrDados['status'] = CI_INATIVO;

            # DELETE
            if($this->ClientsDAO->Update(array('id' => $id), $arrDados)){
                $this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				redirect('Clients/Lists');
            }
        }
	}
}
