<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportDesigner extends CI_Controller {
    public function __construct(){
        parent::__construct();
		checkSession($this);
    }

	public function New(){
		$arrDados['strPagina']    = CI_LABEL_REPORT_DESIGNER.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVO;
		$arrDados['arrClients']   = $this->ClientsDAO->GetAll();
        $arrDados['arrQueryList'] = $this->QueryListDAO->GetAll(array("status" => CI_ATIVO));
		$arrDados['checar']       = null;

		$this->load->view('frmReportDesigner', $arrDados);
	}

	public function Lists(){
		$arrDados['arrDados']  = $this->ReportDesignerDAO->ReportsLists();
		$arrDados['strPagina'] = CI_LABEL_REPORT_DESIGNER.CI_SEPARADOR_NAVEGAR.CI_MENU_CONSULTAR;

        $this->load->view('cntReportDesigner', $arrDados);
	}

	public function Edit(){
		if ($arrDados['arrDados'] = $this->ReportDesignerDAO->GetAll(array('id' => $this->uri->segment(2)))){
			$arrDados['arrDados'][0]['id'] 				   = base64_encode($arrDados['arrDados'][0]['id']);
            $arrDados['arrQueryList'] = $this->QueryListDAO->GetAll(array("status" => CI_ATIVO));
			$arrDados['strPagina'] 		   				   = CI_LABEL_REPORT_DESIGNER.CI_SEPARADOR_NAVEGAR.CI_MENU_EDITAR;
			$arrDados['checar']    		   				   = null;

			if($arrDados['arrDados'][0]['status'] == CI_INATIVO){
				$arrDados['checar'] = CI_CHECKBOX_CHECKED;
			}

			$arrDados['arrClients'] = $this->ClientsDAO->GetAll(array('status' => CI_ATIVO));

			$this->load->view('frmReportDesigner', $arrDados);
		} else {
			$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($this->router->routes['ServiceOrders_lists']);
		}
	}

	public function Save(){
        $arrDados['report_name']         = antinjection($_POST['report_name']);
		$arrDados['client_id']    		 = antinjection($_POST['client_id']);
        $arrDados['user_id']             = $this->session->userdata('session_USU_ID');
		$arrDados['queries']  			 = antinjection(implode(',', $_POST['checklist']));
		$arrDados['created_at']  	     = CI_NOW;
		$arrDados['updated_at']          = CI_NOW;
        $arrDados['status']              = CI_ATIVO;


		if(!empty($_POST['id'])){
			# ATUALIZAÇÃO
			if($this->ReportDesignerDAO->Update(array('id' => antinjection(base64_decode($_POST['id']))), $arrDados)){
				$this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				redirect('ReportDesigner/Lists');
			}
		} else {
			# INCLUSÃO
			if($id = $this->ReportDesignerDAO->InsertId($arrDados)){
				$this->session->set_userdata('success_crud', CI_CADASTRO_OK);

				redirect('ReportDesigner/Lists');
			}
		}
	}

	public function Delete(){
		if($this->uri->segment(2) != null){
            $id                 = $this->uri->segment(2);
            $arrDados['status'] = CI_INATIVO;

            # DELETE
            if($this->ReportDesignerDAO->Update(array('id' => $id), $arrDados)){
                $this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				redirect('ReportDesigner/Lists');
            }
        }
    }
}
