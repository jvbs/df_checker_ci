<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QueryList extends CI_Controller{
    public function __construct(){
        parent::__construct();
		checkSession($this);
    }

	public function News(){
		$arrDados['strPagina'] = CI_LABEL_QUERY_LIST.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVO;
		$arrDados['checar']    = null;
		$this->load->view('frmQueryList', $arrDados);
	}

	public function Lists(){
		$arrDados['arrDados']  = $this->QueryListDAO->GetAll(array('status' => CI_ATIVO));
		$arrDados['strPagina'] = CI_LABEL_QUERY_LIST.CI_SEPARADOR_NAVEGAR.CI_MENU_CONSULTAR;

		$this->load->view('cntQueryList', $arrDados);
	}

	public function Edit(){
		if ($arrDados['arrDados'] = $this->QueryListDAO->GetAll(array('id' => $this->uri->segment(2)))){
			$arrDados['arrDados'][0]['id'] 			= base64_encode($arrDados['arrDados'][0]['id']);
			$arrDados['arrDados'][0]['query_value'] = formatarMoeda($arrDados['arrDados'][0]['query_value']);
			$arrDados['strPagina'] 			        = CI_LABEL_QUERY_LIST.CI_SEPARADOR_NAVEGAR.CI_MENU_EDITAR;
			$arrDados['checar']    			   		= null;

			if ($arrDados['arrDados'][0]['status'] == CI_INATIVO){
				$arrDados['checar'] = CI_CHECKBOX_CHECKED;
			}

			$this->load->view('frmQueryList', $arrDados);
		}else{
			$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($this->router->routes['query_lists']);
		}
	}

	public function Save(){
		$arrDados['user_id']        = $this->session->userdata('session_USU_ID');
		$arrDados['table_type']     = antinjection($_POST['table_type']);
		$arrDados['query_mode']     = antinjection($_POST['query_mode']);
		$arrDados['query_source']   = antinjection($_POST['query_source']);
		$arrDados['query_classify'] = antinjection($_POST['query_classify']);
		$arrDados['query_type']     = antinjection($_POST['query_type']);
		$arrDados['query_value']    = str_replace(",", ".", removeStrings(antinjection($_POST['query_value']), "."));
		$arrDados['query_entries']  = antinjection($_POST['query_entries']);
		$arrDados['query_profile']  = antinjection($_POST['query_profile']);
		$arrDados['link_status']    = antinjection($_POST['link_status']);
		$arrDados['link'] 			= antinjection($_POST['link']);
		$arrDados['status']         = CI_ATIVO;

		if (isset($_POST['status'])) $arrDados['status'] = CI_INATIVO;

		if (!empty($_POST['id'])){
			#ATUALIZAÇÃO			
			if ($this->QueryListDAO->Update(array('id' => antinjection(base64_decode($_POST['id']))), $arrDados)){
				$this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['querylist_news']);
				}else{
					redirect('QueryList/'.base64_decode($_POST['id']));	
				}
			}
		}else{
			#INCLUSÃO
			if ($id = $this->QueryListDAO->InsertId($arrDados)){
				$this->session->set_userdata('success_crud', CI_CADASTRO_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['acoes_novo']);
				}else{
					redirect('QueryList/'.$id);
				}
			}
		}
	}

	public function Delete(){
		$arrJson['sucesso'] = 'false';

		if ($id = $this->uri->segment(2)){				
			if ($this->QueryListDAO->Update(array('id' => $id), array('status' => CI_INATIVO))){					
				$arrJson['sucesso']  = 'true';
				$arrJson['redir']    = base_url($this->router->routes['querylist_list']);
				$this->session->set_userdata('success_crud', CI_EXCLUIDO_OK);
			}
		}

		if ($arrJson['sucesso'] == 'false'){
			$arrJson['redir'] = base_url($this->router->routes['home']);
			$this->session->set_userdata('error_crud', CI_ERRO_PROCESSAMENTO.' ('.__CLASS__.'/'.__FUNCTION__.').');			
		}

		echo json_encode($arrJson);
	}
}