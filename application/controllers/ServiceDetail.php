<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceDetail extends CI_Controller{
    public function __construct(){
        parent::__construct();
		checkSession($this);
    }


    public function Execute(){
        $id = $this->uri->segment(2);
        if($arrDados['arrDados'] = $this->ServiceDetailDAO->ServiceDetailLists(array('id' => $id))){

            $arrDados['strPagina'] = CI_LABEL_SERVICE_ORDERS.CI_SEPARADOR_NAVEGAR.CI_MENU_EXECUTAR;
            $arrDados['arrDados'][0]['is_cpf'] = $arrDados['arrDados'][0]['is_cpf'] == 1 ? 'CPF' : 'CNPJ';
            $arrDados['arrQueries'] = $this->QueryListDAO->QueryLists(array('queries' => $arrDados['arrDados'][0]['queries']));

            $this->load->view('frmServiceDetailExec', $arrDados);
        }
    }

	public function Save(){
		$arrJson['sucesso'] = 'false';
		$arrJson['message'] = CI_ITEM_JA_ADICIONADO;

		#Verifica se já adicionou na ordem de serviço
		if (!$this->ServiceDetailDAO->GetAll(
			array(
				'service_id'    => antinjection(base64_decode($_POST['service_id'])),
				'query_list_id' => antinjection($_POST['query_list_id'])
			)
		)){
			$arrDados['service_id']    = antinjection(base64_decode($_POST['service_id']));
			$arrDados['query_list_id'] = antinjection($_POST['query_list_id']);
			$arrDados['user_id']       = $this->session->userdata('session_USU_ID');

			if ($this->ServiceDetailDAO->Insert($arrDados)){
				$arrJson['sucesso'] = 'true';
			}
		}

		echo json_encode($arrJson);
	}

	// public function Delete(){
	// 	$arrJson['sucesso'] = 'false';
    //
	// 	if ($this->ServiceDetailDAO->Remove(array('service_id' => $this->uri->segment(3), 'query_list_id' =>$this->uri->segment(2)))){
	// 		$arrJson['sucesso']  = 'true';
	// 	}
    //
	// 	echo json_encode($arrJson);
	// }
}
