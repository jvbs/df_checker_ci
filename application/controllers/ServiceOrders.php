<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceOrders extends CI_Controller{
    public function __construct(){
        parent::__construct();
		checkSession($this);
    }

	public function News(){
		$arrDados['strPagina']  = CI_LABEL_SERVICE_ORDERS.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVO;
		$arrDados['arrClients'] = $this->ClientsDAO->GetAll(array('status' => CI_ATIVO));
        $arrDados['arrAnalyst'] = $this->UsersDAO->GetAll(array('status' => CI_ATIVO, 'user_level' => 2));
        $arrDados['arrReports'] = $this->ReportDesignerDAO->GetAll(array('status' => CI_ATIVO));
		$arrDados['checar']     = null;

		$this->load->view('frmServiceOrders', $arrDados);
	}

	public function Lists(){
		$arrDados['arrDados']  = $this->ServiceOrdersDAO->ServiceOrdersLists();
		$arrDados['strPagina'] = CI_LABEL_SERVICE_ORDERS.CI_SEPARADOR_NAVEGAR.CI_MENU_CONSULTAR;

		$this->load->view('cntServiceOrders', $arrDados);
	}

	public function Edit(){
		if ($arrDados['arrDados'] = $this->ServiceOrdersDAO->GetAll(array('id' => $this->uri->segment(3)))){
			$arrDados['arrDados'][0]['id'] 				       = base64_encode($arrDados['arrDados'][0]['id']);
			$arrDados['arrDados'][0]['service_order_date']     = mudarFormatoData($arrDados['arrDados'][0]['service_order_date']);
			$arrDados['arrDados'][0]['service_order_deadline'] = mudarFormatoData($arrDados['arrDados'][0]['service_order_deadline']);
            $arrDados['arrDados'][0]['fat_date']               = mudarFormatoData($arrDados['arrDados'][0]['fat_date']);
			$arrDados['strPagina'] 		   				       = CI_LABEL_SERVICE_ORDERS.CI_SEPARADOR_NAVEGAR.CI_MENU_EXECUTAR;
			$arrDados['checar']    		   				       = null;

			if ($arrDados['arrDados'][0]['status'] == CI_INATIVO){
				$arrDados['checar'] = CI_CHECKBOX_CHECKED;
			}

			$arrDados['arrClients'] = $this->ClientsDAO->GetAll(array('status' => CI_ATIVO));
            $arrDados['arrAnalyst'] = $this->UsersDAO->GetAll(array('status' => CI_ATIVO, 'user_level' => 2));

			$this->load->view('frmServiceOrders', $arrDados);
		}else{
			$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($this->router->routes['ServiceOrders_lists']);
		}
	}

	public function Save(){
        //echo '<pre>' , var_dump($_POST) , '</pre>';
        $arrDados['service_order_id']       = substr(date('YmdHi').rand(),0,15);
		$arrDados['client_id']    		    = antinjection($_POST['client_id']);
		$arrDados['analyst']     	        = antinjection($_POST['analyst_id']);
        $arrDados['service_order_status']   = antinjection($_POST['status-os']);
		$arrDados['service_order_date']     = date('Y-m-d');
		$arrDados['service_order_deadline'] = mudarFormatoData($_POST['service_order_deadline']);
        $arrDados['unit_value']             = str_replace(",", ".", removeStrings(antinjection($_POST['unit-value']), "."));
        $arrDados['full_value']             = str_replace(",", ".", removeStrings(antinjection($_POST['full-value']), "."));
        $arrDados['discount']               = antinjection($_POST['discount']);
        $arrDados['payment_methods']        = antinjection($_POST['payment-methods']);
        $arrDados['nf_number']              = antinjection($_POST['nf-number']);
        $arrDados['fat_date']               = mudarFormatoData($_POST['fat-date']);
        $arrDados['soliciter_name']         = antinjection($_POST['soliciter-name']);
        $arrDados['soliciter_email']        = antinjection($_POST['soliciter-email']);
        $arrDados['soliciter_telephone']    = antinjection($_POST['soliciter-telephone']);
        $arrDados['general_comments']       = antinjection($_POST['general-comments']);
        $arrDados['user_id']                = $this->session->userdata('session_USU_ID');
        $arrDados['last_updated']           = CI_NOW;

        $qtdPesquisas = count($_POST['cpf-cnpj']);

		if(!empty($_POST['id'])){
			# ATUALIZAÇÃO
			if($this->ServiceOrdersDAO->Update(array('id' => antinjection(base64_decode($_POST['id']))), $arrDados)){
				$this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				redirect('ServiceOrders/Lists');
			}
		} else {
			# INCLUSÃO
			if($id = $this->ServiceOrdersDAO->InsertId($arrDados)){
                for($i=0; $i < $qtdPesquisas; $i++){
                    if($id = $this->ServiceDetailDAO->InsertId(array(
                        'service_order_id' => $arrDados['service_order_id'],
                        'analyst' => $arrDados['analyst'],
                        'client_id' => $arrDados['client_id'],
                        'is_cpf' => $_POST['cpf-cnpj'][$i] == 'CPF' ? 1 : 0,
                        'target_id' => $_POST['target_company_id'][$i],
                        'target_name' => $_POST['target_name'][$i],
                        'report_id' => $_POST['report_id'][$i]
                    ))){
                    }
                }
                $this->session->set_userdata('success_crud', CI_CADASTRO_OK);

                redirect('ServiceOrders/Lists');
			}
		}
	}

    public function SaveExecution(){
        $arrDados['query_yes_no']     = $_POST['yes-no'];
        $arrDados['query_comments']   = $_POST['comments'];
        $arrDados['service_order_id'] = $_POST['service_order_id'];
        $arrDados['query_id']         = $_POST['query_id'];
        $arrDados['date_created']     = CI_NOW;
        $arrDados['user_id']          = $this->session->userdata('session_USU_ID');

        $qtdPesquisas = count($arrDados['query_id']);
        echo $qtdPesquisas;
        for($i=0; $i < $qtdPesquisas; $i++){
            $id = $this->ServiceOrdersExecutionDAO->InsertId(array(
                'service_order_id' => $arrDados['service_order_id'],
                'date_created' => $arrDados['date_created'],
                'user_id' => $arrDados['user_id'],
                'query_yes_no' => $_POST['yes-no'][$i] == 'SIM' ? 1 : 0,
                'query_comments' => $_POST['comments'][$i],
                'query_id' => $_POST['query_id'][$i]
            ));
        }
        $this->ServiceOrdersDAO->Update(array('service_order_id' => $arrDados['service_order_id']), array('service_order_status' => 'Finalizada'));

        $this->session->set_userdata('success_crud', CI_CADASTRO_OK);
        redirect('Users/Home');

    }

    public function Delete(){
		if($this->uri->segment(2) != null && $this->uri->segment(3) != null){
            $id                 = $this->uri->segment(2);
            $service_order_id   = $this->uri->segment(3);
            $arrDados['status'] = CI_INATIVO;


            # DELETE
            if($this->ServiceOrdersDAO->Update(array('id' => $id), $arrDados)){
                if($this->ServiceDetailDAO->Update(array('service_order_id' => $service_order_id), $arrDados)){
                    $this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				    redirect('ServiceOrders/Lists');
                }

            }
        }
    }
}
