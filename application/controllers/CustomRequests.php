<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomRequests extends CI_Controller{
    public function __construct(){
        parent::__construct();
    }

    public function New(){
        $arrDados['strPagina'] = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVA_SOLIC_PERSONALIZADA;
        $this->load->view('frmCustomRequests', $arrDados);
    }

    public function Save(){
        $arrDados['request_name']                  = antinjection($_POST['request_name']);
		$arrDados['company_id']    		           = antinjection(removerPontuacoes($_POST['company_id']));
        $arrDados['control_holding']               = isset($_POST['control_holding']) ? '1' : '0';
        $arrDados['partner_shareholder']           = isset($_POST['partner_shareholder']) ? '1' : '0';
        $arrDados['consortium']                    = isset($_POST['consortium']) ? '1' : '0';
        $arrDados['administration_staff']           = isset($_POST['administration_staff']) ? '1' : '0';
        $arrDados['obs']                           = antinjection($_POST['general_information']);
        $arrDados['email']                         = antinjection($_POST['email']);        
        $arrDados['user_id']                       = $this->session->userdata('session_USU_ID');
        $arrDados['created_at']                    = CI_NOW;
        
        
        # INCLUSÃO
		if($id = $this->CustomRequestsDAO->InsertId($arrDados)){
			$this->session->set_userdata('success_crud', CI_CADASTRO_OK);

			redirect('CustomRequests/New');
		}

        # Arq
        // if(isset($_FILES['arquivo'])){
        //     $arquivo = $_FILES['arquivo'];
        //     $ext = end(explode('.', $arquivo));
        //     $config = array(
        //         'upload_path'   => base_url().'assets/uploads',
        //         'file_name'     => $arrDados['request_name'].'.'.$ext,
        //         'max_size'      => '500'
        //     );
        // }
    }
}