<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller{

    public function __construct(){
        parent::__construct();
    }

	public function News(){
		checkSession($this);

		$arrDados['strPagina'] = CI_LABEL_USERS.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVO;
		$this->load->view('frmUsers', $arrDados);
	}

	public function Index(){
		session_start();
		session_destroy();
		$this->load->view('index');
	}

	public function Home(){
		checkSession($this);

		#Quantidade de usuários;
		$arrUsuarios = $this->UsersDAO->GetAll();

        if($this->session->userdata('session_USU_Nivel') == CI_VALUE_USER_NIVEL2){
            $arrDados['arrServiceOrders'] = $this->ServiceDetailDAO->ServiceDetailLists(array('user_id' => $this->session->userdata('session_USU_ID')));
		}
		
		if($this->session->userdata('session_USU_Nivel') == CI_VALUE_USER_NIVEL1){
			$arrDados['arrInfo']   = $this->ServiceOrdersDAO->HomeServiceOrdersInfo();
			$arrDados['arrStatus'] = $this->ServiceOrdersDAO->HomeServiceOrdersChartStatus();
			$arrDados['arrExec']   = $this->ServiceOrdersDAO->HomeServiceOrdersChartExecutions();
        } elseif($this->session->userdata('session_USU_Nivel') == CI_VALUE_USER_NIVEL2) {
			$arrDados['arrInfo']   = $this->ServiceOrdersDAO->HomeServiceOrdersInfo(array('analyst' => $this->session->userdata('session_USU_ID')));
			$arrDados['arrStatus'] = $this->ServiceOrdersDAO->HomeServiceOrdersChartStatus(array('analyst' => $this->session->userdata('session_USU_ID')));
			$arrDados['arrExec']   = $this->ServiceOrdersDAO->HomeServiceOrdersChartExecutions(array('analyst' => $this->session->userdata('session_USU_ID')));
		} else {
			$arrDados['arrInfo']   = $this->ServiceOrdersDAO->HomeServiceOrdersInfo(array('user_id' => $this->session->userdata('session_USU_ID')));
			$arrDados['arrStatus'] = $this->ServiceOrdersDAO->HomeServiceOrdersChartStatus(array('user_id' => $this->session->userdata('session_USU_ID')));
			$arrDados['arrExec']   = $this->ServiceOrdersDAO->HomeServiceOrdersChartExecutions(array('user_id' => $this->session->userdata('session_USU_ID')));
		}

		if ($arrUsuarios){
			$arrDados['intQuantidadeUsuarios'] = (int)count($arrUsuarios);
		}

		#Acessos do dia;
		$intQuantidadeAcessos 			  = rand(1,243);
		$arrDados['intQuantidadeAcessos'] = (int)$intQuantidadeAcessos;

		$this->load->view('home', $arrDados);
	}

	public function CheckEmail(){
		checkSession($this);

		$arrJson['sucesso'] = 'false';

		if (isset($_POST['USU_Email'])){
			$usuariosID = antinjection(base64_decode($_POST['USU_ID']));
			$strEmail   = antinjection($_POST['USU_Email']);

			#Verifica se já existe e-mail cadastrado por GRUPO DE EMPRESA;
			if (valid_email($strEmail)){
				$arrFiltros['USU_Email']  = $strEmail;

				if (!empty($usuariosID)){
					$arrFiltros['USU_IDDiferente'] = $usuariosID;
				}

				$arrChecar = $this->UsersDAO->ConsultarUsuarios($arrFiltros);
				if (count($arrChecar) == 0 || $arrChecar == null){
					$arrJson['sucesso'] = 'true';
				}
			}

			#Verifica se já existe login cadastrado por GRUPO DE EMPRESA;
			$arrChecar = $this->UsersDAO->ConsultarUsuarios($arrFiltros);

			if (count($arrChecar) == 0 || $arrChecar == null){
				$arrJson['sucesso'] = 'true';
			}

		}elseif (isset($_POST['USU_Senha']) && isset($_POST['USU_Senha2'])){
			$strSenha  = antinjection($_POST['USU_Senha']);
			$strSenha2 = antinjection($_POST['USU_Senha2']);

			//Verifica se as senhas são iguais;
			if ($strSenha === $strSenha2) $arrJson['sucesso'] = 'true';
		}

		echo json_encode($arrJson);
	}

	public function Lists(){
		$arrDados['arrDados']  = $this->UsersDAO->GetAll(array('status' => CI_ATIVO));
		$arrDados['strPagina'] = CI_LABEL_USERS.CI_SEPARADOR_NAVEGAR.CI_MENU_CONSULTAR;

		$this->load->view('cntUsers', $arrDados);
	}

	public function Save(){
		$bolNewUpload = false;

		if (isset($_FILES['USU_Imagem']['name'])){
			if (!empty($_FILES['USU_Imagem']['name'])){
				$arrDados['user_profile_pic'] = upload_image($_FILES['USU_Imagem'], CI_UPLOAD_USUARIOS);
				$bolNewUpload = true;
			}
		}

		$arrDados['name']        = antinjection($_POST['name']);
		$arrDados['nickname']    = antinjection($_POST['nickname']);
		$arrDados['email']       = antinjection($_POST['email']);
		$arrDados['company_id']  = antinjection($_POST['company_id']);
		$arrDados['birthdate']   = mudarFormatoData($_POST['birthdate']);
		$arrDados['user_level']  = antinjection(base64_decode($_POST['user_level']));
		$arrDados['status']      = CI_ATIVO;

		if (isset($_POST['status'])) $arrDados['status'] = CI_INATIVO;

		if (!empty($_POST['id'])){
			if (!empty($_POST['USU_ImagemAnterior'])){
				if (isset($_POST['USU_ImagemCheck']) || $bolNewUpload){
					if (is_url_exist(CI_DIRETORIO_USUARIOS.$_POST['USU_ImagemAnterior'])){
						unlink(CI_UPLOAD_USUARIOS.$_POST['USU_ImagemAnterior']);
						if (!isset($arrDados['user_profile_pic'])) $arrDados['USU_Imagem'] = '(NULL)';
					}
				}
			}

			$arrDados['updated_by'] = $this->session->userdata('session_USU_ID');
			$arrDados['updated_at'] = dataHoraAtual();

			if ($this->UsersDAO->Update(array('id' => antinjection(base64_decode($_POST['id']))), $arrDados)){
				if ($this->session->userdata('session_USU_ID') == base64_decode($_POST['id'])){
					if ( !empty($arrDados['user_profile_pic'])){
						$this->session->set_userdata('session_USU_Imagem', '');
						if ($arrDados['user_profile_pic'] != '(NULL)'){
							$this->session->set_userdata('session_USU_Imagem', $arrDados['user_profile_pic']);
						}
					}
				}

				$this->session->set_userdata('success_crud', CI_ATUALIZA_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['users_news']);
				}else{
					redirect('Users/'.base64_decode($_POST['id']));
				}
			}else{
				$this->session->set_userdata('danger_crud', CI_ERRO_PROCESSAMENTO.' ('.__CLASS__.'/'.__FUNCTION__.').');
				redirect($this->router->routes['home']);
			}
		}else{
			$arrDados['created_by'] = $this->session->userdata('session_USU_ID');
			$arrDados['created_at'] = dataHoraAtual();
			$arrDados['password'] 	= md5($_POST['password']);

			if ($id = $this->UsersDAO->InsertId($arrDados)){
				$this->session->set_userdata('success_crud', CI_CADASTRO_OK);

				if (CI_REDIR_NOVO_CADASTRO == true){
					redirect($this->router->routes['users_news']);
				}else{
					redirect('Users/'.$id);
				}
			}else{
				$this->session->set_userdata('danger_crud', CI_ERRO_PROCESSAMENTO.' ('.__CLASS__.'/'.__FUNCTION__.').');
				redirect($this->router->routes['home']);
			}
		}
	}

    public function Login(){
        $cnpj = antinjection($_POST['cnpj']);
        $password = antinjection($_POST['password']);

        $arrayKeys = array(
            "company_id" => removerPontuacoes($cnpj),
            "password" => md5($password)
        );
		
		// var_dump($arrayKeys);

        if($arrUsuarios = $this->UsersDAO->CarregaUsuariosLogin($arrayKeys)){

            #Registra dados do usuário na sessão
            $this->session->set_userdata('session_USU_ID', $arrUsuarios[0]['id']);
            $this->session->set_userdata('session_USU_Nivel', $arrUsuarios[0]['user_level']);


            $this->session->set_userdata('session_USU_Nome', $arrUsuarios[0]['name']);
            $this->session->set_userdata('session_USU_Nickname', $arrUsuarios[0]['nickname']);

            if ($arrUsuarios[0]['user_profile_pic'] == '(NULL)'){
                $arrUsuarios[0]['user_profile_pic'] = '';
            }

            $this->session->set_userdata('session_USU_Imagem', $arrUsuarios[0]['user_profile_pic']);
            $this->session->set_userdata('session_USU_DataNascimento', $arrUsuarios[0]['birthdate']);
            $this->session->set_userdata('session_USU_DataCadastro', $arrUsuarios[0]['created_at']);
            $this->session->set_userdata('session_USU_DataHoraUltimaAtualizacao', $arrUsuarios[0]['updated_at']);
            $this->session->set_userdata('session_USU_Status', $arrUsuarios[0]['status']);

            $this->session->set_userdata('session_USU_Permissoes', $arrUsuarios[0]['permissions']);

            # redirect home page
            redirect($this->router->routes['home']);
        } else {
            $this->session->set_userdata('error_crud', CI_LOGIN_INVALIDO);
            redirect($this->router->routes['default_controller']);
        }
    }

	public function Edit(){
		checkSession($this);

		if ($arrDados['arrDados'] = $this->UsersDAO->GetAll(array('id' => $this->uri->segment(3)))){
			$arrDados['arrDados'][0]['id'] 		  = base64_encode($arrDados['arrDados'][0]['id']);
			$arrDados['arrDados'][0]['birthdate'] = mudarFormatoData($arrDados['arrDados'][0]['birthdate']);

			$arrDados['strPagina'] = CI_LABEL_USERS.CI_SEPARADOR_NAVEGAR.CI_MENU_EDITAR;
			$arrDados['checar']    			   			   = null;

			if ($arrDados['arrDados'][0]['USU_Status'] == CI_INATIVO){
				$arrDados['checar'] = CI_CHECKBOX_CHECKED;
			}

			$this->load->view('frmUsers', $arrDados);
		}else{
			$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
			redirect($this->router->routes['home']);
		}
	}

	public function Delete(){
		checkSession($this);

		$arrJson['sucesso'] = 'false';
		if ($this->LogDAO->LogInsert($this->session->userdata('session_USU_ID'), base64_decode($_POST['FRM_ID2']), __CLASS__.'/'.__FUNCTION__)){
			if ($id = $this->uri->segment(2)){
				if ($this->UsersDAO->Update(array('USU_ID' => $id), array('USU_Status' => CI_INATIVO))){
					$arrJson['sucesso']  = 'true';
					$arrJson['redir']    = base_url($this->router->routes['usuarios_consultar']);
					$this->session->set_userdata('success_crud', $this->session->userdata('session_msg_descricao27'));
				}
			}
		}

		if ($arrJson['sucesso'] == 'false'){
			$arrJson['redir'] = base_url($this->router->routes['home']);
			$this->session->set_userdata('error_crud', CI_ERRO_PROCESSAMENTO.' ('.__CLASS__.'/'.__FUNCTION__.').');
		}

		echo json_encode($arrJson);
	}


	public function GraphicHome(){
		checkSession($this);

		$arrJson['sucesso'] = 'false';

		if ($arrMonths = getMonths()){
			$arrCategories = null;
			$arrDatas 	   = null;

			foreach ($arrMonths as $key => $value){
				$arrCategories[] = $value;
				$intQtdMonth = 0;

				if ($arrDados = $this->UsersDAO->CarregarGraficoUsuariosHome(array('USU_Ano' => antinjection($_POST['USU_Ano']), 'USU_Mes' => $key))){
					$intQtdMonth = $arrDados[0]['TOTAL'];
				}
				$arrDatas[] = (int)$intQtdMonth;
			}

			$arrJson['sucesso']       = 'true';
			$arrJson['arrDatas']      = $arrDatas;
			$arrJson['arrCategories'] = $arrCategories;
		}

		echo json_encode($arrJson);
	}

	public function FormsLoading(){
		$arrJson['sucesso'] = 'true';
		$arrJson['strHtml'] = '';
		$strHtml 			= null;

		$strHtml =
        "<li class='header'>".CI_LABEL_MENU_PRINCIPAL."</li>
			<!-- MENU CLIENTS -->
            <li>
                <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['home'])."');\"><i class='fa fa-home'></i> <span>Dashboard</span></a>
            </li>";
            if(UserHasPermissions($this, 'MANAGE_CLIENTS')){
			$strHtml .=
            "<li class='treeview'>
				<a href=\"javascript: void('".CI_SIGLA."');\">
					<i class='glyphicon glyphicon-user'></i> <span data-toggle='tooltip' title='".CI_LABEL_CLIENTS."'>".CI_LABEL_CLIENTS."</span>
						<span class='pull-right-container'>
							<i class='fa fa-angle-left pull-right'></i>
						</span>
				</a>
			<ul class='treeview-menu' id='item1'>
				<li class='treeview'>
					<a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['clients_news'])."');\">
						<i class='fa fa-plus'></i> Novo Cliente
					</a>
				</li>
				<li class='treeview'>
					<a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['clients_lists'])."');\">
						<i class='fa fa-pencil'></i> Editar Clientes
					</a>
				</li>
			</ul>";
        }
		$strHtml .=	"<!-- QUERY LIST -->
			<!-- <li class='treeview'>
				<a href=\"javascript: void('".CI_SIGLA."');\">
					<i class='glyphicon glyphicon-th-list'></i> <span data-toggle='tooltip' title='".CI_LABEL_QUERY_LIST."'>".CI_LABEL_QUERY_LIST."</span>
						<span class='pull-right-container'>
							<i class='fa fa-angle-left pull-right'></i>
						</span>
				</a>
			<ul class='treeview-menu' id='item2'>
				<li class='treeview'>
					<a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['querylist_news'])."');\">
						<i class='fa fa-folder'></i>".CI_MENU_NOV."
					</a>
				</li>
				<li class='treeview'>
					<a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['querylist_lists'])."');\">
						<i class='fa fa-table'></i>".CI_MENU_CONSULTA."
					</a>
				</li>
			</ul> -->";
			if(UserHasPermissions($this, 'DIRECT_REQUEST')){
				$strHtml.=
				"<li class='treeview menu'>
					<a href='#'><i class='fa fa-check-square-o'></i> Solicitações
					<span class='pull-right-container'>
						<i class='fa fa-angle-left pull-right'></i>
					</span>
					</a>
					<ul class='treeview-menu' style='display: block;'>
					<li class='treeview'>
						<a href='#'><i class='fa fa-circle-o'></i> Direta
						<span class='pull-right-container'>
							<i class='fa fa-angle-left pull-right'></i>
						</span>
						</a>
						<ul class='treeview-menu'>
							<li><a href='#' onClick=\"carregarTela('".site_url($this->router->routes['direct_requests_new'])."');\"><i class='fa fa-plus'></i> Novo</a></li>
							<li><a href='#' onClick=\"carregarTela('".site_url($this->router->routes['direct_requests_lists'])."');\"><i class='fa fa-pencil'></i> Editar</a></li>
						</ul>
					</li>
					<li class='treeview'>
						<a href='#'><i class='fa fa-circle-o'></i> Personalizada
						<span class='pull-right-container'>
							<i class='fa fa-angle-left pull-right'></i>
						</span>
						</a>
						<ul class='treeview-menu'>
						<li><a href='#' onClick=\"carregarTela('".site_url($this->router->routes['custom_requests_new'])."');\"><i class='fa fa-plus'></i> Novo</a></li>
						<li><a href='#' onClick=\"carregarTela('".site_url($this->router->routes['custom_requests_lists'])."');\"><i class='fa fa-pencil'></i> Editar</a></li>
						</ul>
					</li>
					</ul>
			  	</li>";
				}
			if($this->session->userdata('session_USU_Nivel') <> 3){
			$strHtml.=
			"<!-- SERVICE ORDERS -->
			<li class='treeview'>
				<a href=\"javascript: void('".CI_SIGLA."');\">
					<i class='glyphicon glyphicon-file'></i> <span data-toggle='tooltip' title='".CI_LABEL_PCP."'>".CI_LABEL_PCP."</span>
					<span class='pull-right-container'>
						<i class='fa fa-angle-left pull-right'></i>
					</span>
				</a>

                <ul class='treeview-menu' id='item3'>";
                
                if(UserHasPermissions($this, 'SERVICE_ORDER')){
                $strHtml.=
                "<li class='treeview'>
                    <a href='#'><i class='fa fa-table'></i> Ordem de Serviço
                        <span class='pull-right-container'>
                            <i class='fa fa-angle-left pull-right'></i>
                        </span>
                    </a>
                    <ul class='treeview-menu' style='display: none;'>
                        <li>
                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['serviceorders_news'])."');\">
                                <i class='fa fa-plus'></i> Novo
                            </a>
                        </li>
                        <li>
                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['serviceorders_lists'])."');\">
                                <i class='fa fa-pencil'></i> Editar
                            </a>
                        </li>
                    </ul>
                </li>";
				}}
				
			$strHtml.=
            "</ul>
            </li>
            <li>
                <a><i class='fa fa-area-chart'></i> <span>Relatórios</span></a>
			</li>";
			if($this->session->userdata('session_USU_Nivel') <> 3){
			$strHtml.="
			<!-- USERS -->
            <li class='header'>Administrativo</li>
                        <li class='treeview'>
                            <a href='#'>
                                <i class='fa fa-cog'></i> <span>Configurações</span>
                                <span class='pull-right-container'>
                                    <i class='fa fa-angle-left pull-right'></i>
                                </span>
                            </a>
                            <ul class='treeview-menu' style='display: none;'>
                                <!--<li>
                                    <a href='#'><i class='fa fa-user'></i> Meus Dados</a>
                                </li>-->";
                                if(UserHasPermissions($this, 'QUERY_LIST')){
                                $strHtml.=
                                "<li class='treeview'>
                                    <a href='#'><i class='fa fa-table'></i> Critérios de Pesquisa
                                        <span class='pull-right-container'>
                                            <i class='fa fa-angle-left pull-right'></i>
                                        </span>
                                    </a>
                                    <ul class='treeview-menu' style='display: none;'>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['querylist_news'])."');\">
                                                <i class='fa fa-plus'></i> Novo
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['querylist_lists'])."');\">
                                                <i class='fa fa-pencil'></i> Editar
                                            </a>
                                        </li>
                                        <!--<li class='treeview'>
                                            <a href='#'><i class='fa fa-circle-o'></i> Level Two
                                                <span class='pull-right-container'>
                                                    <i class='fa fa-angle-left pull-right'></i>
                                                </span>
                                            </a>
                                            <ul class='treeview-menu' style='display: none;'>
                                                <li><a href='#'><i class='fa fa-circle-o'></i> Level Three</a></li>
                                                <li><a href='#'><i class='fa fa-circle-o'></i> Level Three</a></li>
                                            </ul>
                                        </li>-->
                                    </ul>
                                </li>";
                                }
                                if(UserHasPermissions($this, 'REPORT_DESIGNER')){
                                $strHtml.=
                                "<li class='treeview'>
                                    <a href='#'><i class='fa fa-list-alt'></i> Designer de Relatórios
                                        <span class='pull-right-container'>
                                            <i class='fa fa-angle-left pull-right'></i>
                                        </span>
                                    </a>
                                    <ul class='treeview-menu' style='display: none;'>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['report_designer_new'])."');\">
                                                <i class='fa fa-plus'></i> Novo
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['report_designer_lists'])."');\">
                                                <i class='fa fa-pencil'></i> Editar
                                            </a>
                                        </li>
                                        <!--<li class='treeview'>
                                            <a href='#'><i class='fa fa-circle-o'></i> Level Two
                                                <span class='pull-right-container'>
                                                    <i class='fa fa-angle-left pull-right'></i>
                                                </span>
                                            </a>
                                            <ul class='treeview-menu' style='display: none;'>
                                                <li><a href='#'><i class='fa fa-circle-o'></i> Level Three</a></li>
                                                <li><a href='#'><i class='fa fa-circle-o'></i> Level Three</a></li>
                                            </ul>
                                        </li>-->
                                    </ul>
                                </li>";
                                }
                                if(UserHasPermissions($this, 'MANAGE_USERS')){
                                $strHtml.="<li class='treeview'>
                                    <a href='#'><i class='fa fa-users'></i> Usuários
                                        <span class='pull-right-container'>
                                            <i class='fa fa-angle-left pull-right'></i>
                                        </span>
                                    </a>
                                    <ul class='treeview-menu' style='display: none;'>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['users_news'])."');\">
                                                <i class='fa fa-plus'></i> Novo
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"javascript: void('".CI_SIGLA."');\" onClick=\"carregarTela('".site_url($this->router->routes['users_lists'])."');\">
                                                <i class='fa fa-pencil'></i> Editar
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <!--<li><a href='#'><i class='fa fa-circle-o'></i> Level One</a></li>-->
                            </ul>
                        </li>";}}

		$arrJson['strHtml'] = $strHtml;

		echo json_encode($arrJson);
	}
}
