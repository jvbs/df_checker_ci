<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DirectRequests extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }

    public function New(){
        $arrDados['strPagina'] = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_NOVA_SOLIC_DIRETA;
        $this->load->view('frmDirectRequest', $arrDados);
    }

    public function Lists(){
        $arrDados['arrDados']  = $this->ServiceOrdersDAO->ServiceOrdersLists(array('user_id' => $this->session->userdata('session_USU_ID')));
		$arrDados['strPagina'] = CI_LABEL_CLIENTS.CI_SEPARADOR_NAVEGAR.CI_MENU_EDITAR_SOLIC_DIRETA;

		$this->load->view('cntDirectRequests', $arrDados);
    }

    // public function Edit(){
	// 	if ($arrDados['arrDados'] = $this->ServiceOrdersDAO->GetAll(array('id' => $this->uri->segment(3)))){
	// 		$arrDados['arrDados'][0]['id'] 				       = base64_encode($arrDados['arrDados'][0]['id']);
	// 		$arrDados['arrDados'][0]['service_order_date']     = mudarFormatoData($arrDados['arrDados'][0]['service_order_date']);
	// 		$arrDados['arrDados'][0]['service_order_deadline'] = mudarFormatoData($arrDados['arrDados'][0]['service_order_deadline']);
    //         $arrDados['arrDados'][0]['fat_date']               = mudarFormatoData($arrDados['arrDados'][0]['fat_date']);
	// 		$arrDados['strPagina'] 		   				       = CI_LABEL_SERVICE_ORDERS.CI_SEPARADOR_NAVEGAR.CI_MENU_EXECUTAR;
	// 		$arrDados['checar']    		   				       = null;

	// 		if ($arrDados['arrDados'][0]['status'] == CI_INATIVO){
	// 			$arrDados['checar'] = CI_CHECKBOX_CHECKED;
	// 		}

	// 		$arrDados['arrClients'] = $this->ClientsDAO->GetAll(array('status' => CI_ATIVO));
    //         $arrDados['arrAnalyst'] = $this->UsersDAO->GetAll(array('status' => CI_ATIVO, 'user_level' => 2));

	// 		$this->load->view('frmServiceOrders', $arrDados);
	// 	}else{
	// 		$this->session->set_userdata('danger_crud', CI_ACCESS_DENIED.' ('.__CLASS__.'/'.__FUNCTION__.').');
	// 		redirect($this->router->routes['ServiceOrders_lists']);
	// 	}
	// }


    public function Save(){
        //echo '<pre>' , var_dump($_POST) , '</pre>';
        $arrDados['service_order_id']       = substr(date('YmdHi').rand(),0,15);
		$arrDados['client_id']    		    = $this->session->userdata('session_USU_ID');
		// $arrDados['analyst']     	        = antinjection($_POST['analyst_id']);
        $arrDados['service_order_status']   = "Aguardando execução";
		$arrDados['service_order_date']     = date('Y-m-d');
		// $arrDados['service_order_deadline'] = mudarFormatoData($_POST['service_order_deadline']);
        // $arrDados['unit_value']             = str_replace(",", ".", removeStrings(antinjection($_POST['unit-value']), "."));
        $arrDados['full_value']             = str_replace(",", ".", removeStrings(antinjection($_POST['full-value']), "."));
        // $arrDados['discount']               = antinjection($_POST['discount']);
        // $arrDados['payment_methods']        = antinjection($_POST['payment-methods']);
        // $arrDados['nf_number']              = antinjection($_POST['nf-number']);
        // $arrDados['fat_date']               = mudarFormatoData($_POST['fat-date']);
        // $arrDados['soliciter_name']         = antinjection($_POST['soliciter-name']);
        $arrDados['soliciter_email']        = antinjection($_POST['soliciter-email']);
        // $arrDados['soliciter_telephone']    = antinjection($_POST['soliciter-telephone']);
        // $arrDados['general_comments']       = antinjection($_POST['general-comments']);
        $arrDados['user_id']                = $this->session->userdata('session_USU_ID');
        $arrDados['last_updated']           = CI_NOW;
        $arrDados['deadline_status']        = $_POST['deadline_status'];

        $qtdPesquisas = count($_POST['cpf-cnpj']);

        // var_dump($arrDados);
		if($id = $this->ServiceOrdersDAO->InsertId($arrDados)){
            for($i=0; $i < $qtdPesquisas; $i++){
                if($id = $this->ServiceDetailDAO->InsertId(array(
                    'service_order_id' => $arrDados['service_order_id'],
                    'analyst' => $arrDados['analyst'],
                    'client_id' => $arrDados['client_id'],
                    'is_cpf' => $_POST['cpf-cnpj'][$i] == 'CPF' ? 1 : 0,
                    'target_id' => $_POST['target_company_id'][$i],
                    'target_name' => $_POST['target_name'][$i],
                    'report_id' => $_POST['report_id'][$i]
                ))){
                }
            }
            $this->session->set_userdata('success_crud', CI_CADASTRO_OK);
            redirect('DirectRequests/Lists');
		}
	}
}