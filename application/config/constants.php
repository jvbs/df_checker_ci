<?php
//echo '<pre>'; print_r($_SERVER); exit;
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

#DEFINIÇÕES DE CONSTANTES DO PROJETO
defined('CI_SIGLA') OR define('CI_SIGLA', 'DF');
defined('CI_NOME_PROJETO') OR define('CI_NOME_PROJETO', 'DF Checker');
defined('CI_ERRO_PROCESSAMENTO') OR define('CI_ERRO_PROCESSAMENTO', 'Ocorreu um erro neste processo. Consulte o Administrador do Sistema');
defined('CI_LOGIN_INVALIDO') OR define('CI_LOGIN_INVALIDO', 'Usuário ou senha inválidos.');
defined('CI_ACCESS_DENIED') OR define('CI_ACCESS_DENIED', 'Acesso Negado');
defined('CI_TITULO_PAGINA') OR define('CI_TITULO_PAGINA', CI_NOME_PROJETO);
defined('CI_DESCRICAO_PAGINA') OR define('CI_DESCRICAO_PAGINA', CI_NOME_PROJETO);
defined('CI_AUTOR_PAGINA') OR define('CI_AUTOR_PAGINA', 'Diego Raphael <diegoraphael.php@gmail.com>');
defined('CI_PALAVRA_CHAVE_PAGINA') OR define('CI_PALAVRA_CHAVE_PAGINA', 'Sistema de Gerenciamento de Projetos, PHP, MySQL, Bootstrap, JQuery, Javascript, HTML, CSS');
defined('CI_SELECIONE') OR define('CI_SELECIONE', '-- SELECIONE --');
defined('CI_NOVO') OR define('CI_NOVO', '-- NOVO --');
defined('CI_TODOS') OR define('CI_TODOS', '-- TODOS --');
defined('CI_CARREGANDO') OR define('CI_CARREGANDO', 'Carregando...');
defined('CI_CARREGANDO_COR') OR define('CI_CARREGANDO_COR', "<font style=\"color:#FFFFFF;\">Carregando...</font>");
defined('CI_ENVIANDO') OR define('CI_ENVIANDO', 'Enviando...');
defined('CI_INFO_ALERT') OR define('CI_INFO_ALERT', 'Informação');
defined('CI_INFO_ERROR') OR define('CI_INFO_ERROR', 'Atenção!');
defined('CI_CODE_ERROR') OR define('CI_CODE_ERROR', 5);
defined('CI_CODE_ALERT') OR define('CI_CODE_ALERT', 3);
defined('CI_SIM') OR define('CI_SIM', 'S');
defined('CI_NAO') OR define('CI_NAO', 'N');
defined('CI_ORDERBY_ASC') OR define('CI_ORDERBY_ASC', 'ASC');
defined('CI_ORDERBY_DESC') OR define('CI_ORDERBY_DESC', 'DESC');
defined('CI_LABEL_SIM') OR define('CI_LABEL_SIM', 'Sim');
defined('CI_LABEL_NAO') OR define('CI_LABEL_NAO', 'Não');
defined('CI_LABEL_USERS') OR define('CI_LABEL_USERS', 'Usuários');
defined('CI_LABEL_PCP') OR define('CI_LABEL_PCP', 'PCP');
defined('CI_LABEL_CLIENTS') OR define('CI_LABEL_CLIENTS', 'Clientes');
defined('CI_LABEL_QUERY_LIST') OR define('CI_LABEL_QUERY_LIST', 'Critérios de Pesquisa');
defined('CI_LABEL_SERVICE_ORDERS') OR define('CI_LABEL_SERVICE_ORDERS', 'Ordem de Serviço');
defined('CI_LABEL_SERVICE_DETAIL') OR define('CI_LABEL_SERVICE_DETAIL', 'Itens de Ordem de Derviço');
defined('CI_LABEL_REPORT_DESIGNER') OR define('CI_LABEL_REPORT_DESIGNER', 'Designer de Relatórios');
defined('CI_LOGIN') OR define('CI_LOGIN', CI_NOME_PROJETO);
defined('CI_PLACEHOLDER_PESQUISAR') OR define('CI_PLACEHOLDER_PESQUISAR', 'Pesquisar...');
defined('CI_DIRETORIO_PROJETO') OR define('CI_DIRETORIO_PROJETO', 'http://'.$_SERVER['HTTP_HOST'].'/checker/');
defined('CI_FAVICON') OR define('CI_FAVICON', CI_DIRETORIO_PROJETO.'/assets/images/favicon.png');
defined('CI_DIRETORIO_USUARIOS') OR define('CI_DIRETORIO_USUARIOS', CI_DIRETORIO_PROJETO."assets/images/uploads/usuarios/");
defined('CI_DIRETORIO_IMAGENS') OR define('CI_DIRETORIO_IMAGENS', CI_DIRETORIO_PROJETO."assets/images/");
defined('CI_UPLOAD_USUARIOS') OR define('CI_UPLOAD_USUARIOS', "assets/images/uploads/usuarios/");
defined('CI_ATIVO') OR define('CI_ATIVO', 'A');
defined('CI_INATIVO') OR define('CI_INATIVO', 'I');
defined('CI_LABEL_INATIVO') OR define('CI_LABEL_INATIVO', 'Inativo');
defined('CI_LABEL_ATIVO') OR define('CI_LABEL_ATIVO', 'Ativo');
defined('CI_LABEL_BTN_CADASTRAR') OR define('CI_LABEL_BTN_CADASTRAR', 'Cadastrar');
defined('CI_LABEL_BTN_CANCELAR') OR define('CI_LABEL_BTN_CANCELAR', 'Cancelar');
defined('CI_LABEL_BTN_ADICIONAR') OR define('CI_LABEL_BTN_ADICIONAR', 'Adicionar');
defined('CI_LABEL_BTN_ITENS') OR define('CI_LABEL_BTN_ITENS', 'Itens');
defined('CI_MENSAGEM_CPFCNPJ_INVALIDO') OR define('CI_MENSAGEM_CPFCNPJ_INVALIDO', 'CPF/CNPJ inválido.');
defined('CI_MENSAGEM_CPFCNPJ_JA_EXISTE') OR define('CI_MENSAGEM_CPFCNPJ_JA_EXISTE', 'CPF/CNPJ já existe cadastrado na nossa base de dados.');
defined('CI_PLACEHOLDER_DIGITE_AQUI') OR define('CI_PLACEHOLDER_DIGITE_AQUI', 'Digite aqui...');
defined('CI_SEM_DADOS') OR define('CI_SEM_DADOS', 'Não há dados para visualização.');
defined('CI_FADEOUT') OR define('CI_FADEOUT', 7000);
defined('CI_SETTIMEOUT') OR define('CI_SETTIMEOUT', 15000);
defined('CI_SETINTERVAL') OR define('CI_SETINTERVAL', 10000);
defined('CI_VERSION') OR define('CI_VERSION', '<b>Versão</b> 1.0');
defined('CI_COPYRIGHT') OR define('CI_COPYRIGHT', '<b>Todos os direitos reservados &copy; 2018 <a data-toggle="tooltip" target="_blank" href="#">'.CI_NOME_PROJETO.'</a>.</b>');
defined('CI_LABEL_MENU_PRINCIPAL') OR define('CI_LABEL_MENU_PRINCIPAL', 'Menu Principal');
defined('CI_LABEL_PAINEL_CONTROLE') OR define('CI_LABEL_PAINEL_CONTROLE', 'Painel de Controle');
defined('CI_LABEL_UPLOAD_FILE') OR define('CI_LABEL_UPLOAD_FILE', 'Selecione o arquivo...');
defined('CI_LABEL_INICIO') OR define('CI_LABEL_INICIO', 'Início');
defined('CI_LABEL_OPERACOES') OR define('CI_LABEL_OPERACOES', 'Operações');
defined('CI_LABEL_LOGIN_FORGOT_PASS') OR define('CI_LABEL_LOGIN_FORGOT_PASS', 'Esqueci a senha');
defined('CI_LABEL_LOGIN_NEW_USER') OR define('CI_LABEL_LOGIN_NEW_USER', 'Registrar novo membro');
defined('CI_SQL_LIKE') OR define('CI_SQL_LIKE', '%');
defined('CI_TITLE_EDITAR') OR define('CI_TITLE_EDITAR', 'Editar');
defined('CI_TITLE_EXCLUIR') OR define('CI_TITLE_EXCLUIR', 'Excluir');
defined('CI_PAGINACAO') OR define('CI_PAGINACAO', '5, 10, 20, 50, 100, 200');
defined('CI_LABEL_PESSOA_FISICA') OR define('CI_LABEL_PESSOA_FISICA', 'Física');
defined('CI_LABEL_PESSOA_JURIDICA') OR define('CI_LABEL_PESSOA_JURIDICA', 'Jurídica');
defined('CI_FLAG_PESSOA_FISICA') OR define('CI_FLAG_PESSOA_FISICA', 'F');
defined('CI_FLAG_PESSOA_JURIDICA') OR define('CI_FLAG_PESSOA_JURIDICA', 'J');
defined('CI_LABEL_CPF') OR define('CI_LABEL_CPF', 'CPF');
defined('CI_LABEL_CNPJ') OR define('CI_LABEL_CNPJ', 'CNPJ');
defined('CI_TIPO_PERFIL_ADM') OR define('CI_TIPO_PERFIL_ADM', 1);
defined('CI_SELECT_OBRIGATORIO') OR define('CI_SELECT_OBRIGATORIO', 'required');
defined('CI_REDIR_NOVO_CADASTRO') OR define('CI_REDIR_NOVO_CADASTRO', true);
defined('CI_ACAO_CONSULTAR') OR define('CI_ACAO_CONSULTAR', 'Lists');
defined('CI_ACAO_SALVAR') OR define('CI_ACAO_SALVAR', 'Save');
defined('CI_ACAO_EDITAR') OR define('CI_ACAO_EDITAR', 'Edit');
defined('CI_ACAO_EXCLUIR') OR define('CI_ACAO_EXCLUIR', 'Delete');
defined('CI_LABEL_CONFIRMAR_EXCLUSAO') OR define('CI_LABEL_CONFIRMAR_EXCLUSAO', 'Excluir');
defined('CI_LABEL_CONFIRMAR_CANCELAR') OR define('CI_LABEL_CONFIRMAR_CANCELAR', 'Cancelar');
defined('CI_LABEL_CONFIRMAR_OK') OR define('CI_LABEL_CONFIRMAR_OK', 'Ok');
defined('CI_LABEL_CONFIRMAR_DESCRICAO') OR define('CI_LABEL_CONFIRMAR_DESCRICAO', 'Descrição: ');
defined('CI_LABEL_CONFIRMAR_NOME') OR define('CI_LABEL_CONFIRMAR_NOME', 'Nome: ');
defined('CI_LABEL_GRAFICO_USUARIOS') OR define('CI_LABEL_GRAFICO_USUARIOS', 'Gráfico Usuários Cadastrados');
defined('CI_MENU_NOVO') OR define('CI_MENU_NOVO', 'Novo');
defined('CI_MENU_NOVA_SOLIC_DIRETA') OR define('CI_MENU_NOVA_SOLIC_DIRETA', 'Nova Solicitação Direta');
defined('CI_MENU_EDITAR_SOLIC_DIRETA') OR define('CI_MENU_EDITAR_SOLIC_DIRETA', 'Editar Solicitação Direta');
defined('CI_MENU_NOVA_SOLIC_PERSONALIZADA') OR define('CI_MENU_NOVA_SOLIC_PERSONALIZADA', 'Nova Solicitação Personalizada');
defined('CI_MENU_EDITAR_SOLIC_PERSONALIZADA') OR define('CI_MENU_EDITAR_SOLIC_PERSONALIZADA', 'Editar Solicitação Personalizada');
defined('CI_MENU_EDITAR') OR define('CI_MENU_EDITAR', 'Editar');
defined('CI_MENU_CONSULTAR') OR define('CI_MENU_CONSULTAR', 'Consultar');
defined('CI_MENU_EXECUTAR') OR define('CI_MENU_EXECUTAR', 'Executar');
defined('CI_SEPARADOR_NAVEGAR') OR define('CI_SEPARADOR_NAVEGAR', ' > ');
defined('CI_CADASTRO_OK') OR define('CI_CADASTRO_OK', 'Registro cadastrado com sucesso.');
defined('CI_ATUALIZA_OK') OR define('CI_ATUALIZA_OK', 'Registro atualizado com sucesso.');
defined('CI_EXCLUIDO_OK') OR define('CI_EXCLUIDO_OK', 'Registro excluido com sucesso.');
defined('CI_CHECKBOX_CHECKED') OR define('CI_CHECKBOX_CHECKED', 'checked');
defined('CI_SELECT_SELECTED') OR define('CI_SELECT_SELECTED', 'selected');
defined('CI_LABEL_USER_NIVEL1') OR define('CI_LABEL_USER_NIVEL1', 'Nível 1');
defined('CI_LABEL_USER_NIVEL2') OR define('CI_LABEL_USER_NIVEL2', 'Nível 2');
defined('CI_LABEL_USER_NIVEL3') OR define('CI_LABEL_USER_NIVEL3', 'Nível 3');
defined('CI_VALUE_USER_NIVEL1') OR define('CI_VALUE_USER_NIVEL1', 1);
defined('CI_VALUE_USER_NIVEL2') OR define('CI_VALUE_USER_NIVEL2', 2);
defined('CI_NOW') OR define('CI_NOW', date('Y-m-d H:i:s'));
defined('CI_VALUE_USER_NIVEL3') OR define('CI_VALUE_USER_NIVEL3', 3);
defined('CI_LABEL_REMOVER_IMAGEM') OR define('CI_LABEL_REMOVER_IMAGEM', 'Remover Imagem');
defined('CI_ITEM_JA_ADICIONADO') OR define('CI_ITEM_JA_ADICIONADO', 'Item já adicionado a ordem de serviço');
