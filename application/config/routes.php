<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']   = 'Users';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;
$route['home']          	   = "Users/Home";

# Users
$route['users_login']              = "Users/Login";
$route['users_news']               = "Users/News";
$route['users_save']               = "Users/Save";
$route['users_lists']              = "Users/Lists";
$route['users_check_email']        = "Users/CheckEmail";
$route['users_edit/(:num)']        = "Users/Edit/$1";
$route['users_delete/(:num)']      = "Users/Delete/$1";
$route['users_generate_graphics']  = "Users/GraphicHome";
$route['users_forms_loading']      = "Users/FormsLoading";
$route['users_logout']             = "Users/Index";

# Clients
$route['clients_news']                = "Clients/News";
$route['clients_save']                = "Clients/Save";
$route['clients_lists']               = "Clients/Lists";
$route['clients_edit/(:num)']         = "Clients/Edit/$1";
$route['clients_delete/(:num)']       = "Clients/Delete/$1";

# Query Lists
$route['querylist_news']           = "QueryList/News";
$route['querylist_save']           = "QueryList/Save";
$route['querylist_lists']          = "QueryList/Lists";
$route['querylist_edit/(:num)']    = "QueryList/Edit/$1";
$route['querylist_delete/(:num)']  = "QueryList/Delete/$1";

# Service Order
$route['serviceorders_news']                  = "ServiceOrders/News";
$route['serviceorders_save']                  = "ServiceOrders/Save";
$route['serviceorders_save_execution']        = "ServiceOrders/SaveExecution";
$route['serviceorders_lists']                 = "ServiceOrders/Lists";
$route['serviceorders_edit/(:num)']           = "ServiceOrders/Edit/$1";
$route['serviceorders_delete/(:num)/(:any)']  = "ServiceOrders/Delete/$1/$2";

# Service Detail
$route['servicedetail_news']          	     = "ServiceDetail/News";
$route['servicedetail_delete/(:num)/(:any)'] = "ServiceDetail/Delete/$1/$2";
$route['servicedetail_lists']                = "ServiceDetail/Lists";
$route['servicedetail_save']          		 = "ServiceDetail/Save";
$route['servicedetail_execute/(:num)']       = "ServiceDetail/Execute/$1";

# Report Designer
$route['report_designer_new']                  = "ReportDesigner/New";
$route['report_designer_delete/(:num)']        = "ReportDesigner/Delete/$1";
$route['report_designer_lists']                = "ReportDesigner/Lists";
$route['report_designer_edit/(:num)']          = "ReportDesigner/Edit/$1";
$route['report_designer_save']                 = "ReportDesigner/Save";

# Direct Requests
$route['direct_requests_new']   = "DirectRequests/New";
$route['direct_requests_lists'] = "DirectRequests/Lists";
$route['direct_requests_save']  = "DirectRequests/Save";

# Custom Requests & Custom Requests Responses
$route['custom_requests_new']   = "CustomRequests/New";
$route['custom_requests_lists'] = "CustomRequests/Lists";
$route['custom_requests_save']  = "CustomRequests/Save";
