<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);


$autoload['packages']  = array();
$autoload['libraries'] = array('database', 'session', 'form_validation');
$autoload['drivers']   = array();
$autoload['helper']    = array('url', 'security', 'string', 'select', 'date', 'images', 'email', 'path', 'number', 'form');
$autoload['config']    = array();
$autoload['language']  = array();
$autoload['model']     = array('ClientsDAO', 'QueryListDAO', 'ServiceDetailDAO', 'ServiceOrdersDAO', 'UsersDAO', 'ReportDesignerDAO', 'ServiceOrdersExecutionDAO', 'CustomRequestsDAO');
