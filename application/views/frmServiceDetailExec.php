<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <form class="form-horizontal" id="frmFormulario" method="post" action="<?=base_url($this->router->routes['serviceorders_save_execution'])?>" enctype="multipart/form-data">
				<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
				<div class="box-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group form-group-sm" style="margin-right:5px">
                                    <label class="control-label" for="USU_Nome">Número da OS</label>
                                    <input type="text" name="service_order_id" value="<?=$arrDados[0]['service_order_id']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-sm" style="margin-right:5px">
    							  <label class="control-label" for="USU_Nome">Cliente</label>
    								<input type="text" name="client_id" value="<?=$arrDados[0]['fantasy_name']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
    							</div>
                            </div>

                            <div class="col-md-3">
    							<div class="form-group form-group-sm" style="margin-right:5px">
    							  <label class="control-label" for="analyst">Analista</label>
    								<input type="text" name="analyst" value="<?=$arrDados[0]['name']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
    							</div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group form-group-sm" style="margin-right:5px">
                                    <label class="control-label">Status da OS</label>
                                    <input type="text" name="service_order_status" value="<?=$arrDados[0]['service_order_status']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-2">
    							<div class="form-group form-group-sm" style="margin-right:5px">
    							     <label class="control-label">Prazo de Entrega</label>
    								  <input type="text" name="service_order_deadline" value="<?=mudarFormatoData($arrDados[0]['service_order_deadline'])?>" autocomplete="off" id="deadline" placeholder="" class="form-control hasDatepicker" readonly=readonly>
    							</div>
                            </div>
                        </div>
                        <div class="row" style="border-top:1px solid lightgrey;border-bottom:1px solid lightgrey">
                            <div class="start-search" style="margin-left:15px; margin-top: 20px">
                                <div class="search-block">
                                    <div class="this-block" style="margin-bottom: 25px">


                                    <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group form-group-sm">
                                             <input type="text" style="margin-top: 23px" name="is_cpf" value="<?=$arrDados[0]['is_cpf']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-sm">
                                            <label class="control-label" for="target_company_id">CPF/CNPJ</label>
                                            <input type="text" name="target_company_id[]" id="target_company_id" autocomplete="off" value="<?=$arrDados[0]['target_id']?>" placeholder="" maxlength="14" class="form-control" readonly="readonly"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Pesquisado</label>
                						        <input type="text" name="target_company_id[]" id="target_company_id" autocomplete="off" value="<?=$arrDados[0]['target_name']?>" placeholder="" maxlength="14" class="form-control" readonly="readonly"/>
                						</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Relatório</label>
                						    <input type="text" name="target_company_id[]" id="target_company_id" autocomplete="off" value="<?=$arrDados[0]['report_name']?>" placeholder="" maxlength="14" class="form-control" readonly="readonly"/>
                						</div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                            if(isset($arrQueries)){
                                for($i=0; $i < count($arrQueries); $i++){
                                    echo "
                                    <div class='col-lg-12' style='margin:5px 0px 10px -20px'>
                                        <label class='control-label' style='margin-bottom: 10px;text-align:left'>{$arrQueries[$i]['query_description']} <a href='{$arrQueries[$i]['link']}' target='_blank'><i class='fa fa-globe'></i></a></label>
                                        <input type='hidden' name='query_id[]' value='{$arrQueries[$i]['id']}'>
                                        <select class='form-control' name='yes-no[]' style='margin-bottom: 10px'>
                                            <option value='1'>SIM</option>
                                            <option value='0'>NÃO</option>
                                        </select>
                                        <textarea name='comments[]' class='form-control' placeholder='Insira seus comentários aqui' rows='8'></textarea>
                                    </div>";
                                }
                            }
                        ?>

                    </div>
                    <div class="box-footer">
                            <button type="button" class="btn btn-default" id="btnCancelar">
                                <i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
                            </button>
                            <button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
                                <i class="glyphicon glyphicon-ok-circle"></i>
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
