<?php

$this->load->view('commons/cabecalho-logado.php');

// var_dump($arrStatus);
if(UserHasPermissions($this, 'ANALYST_HOMESCREEN')){
?>

    <section class="content" id="divContainer">
      <div class="row" style="margin-top:25px">
        <div class="col-lg-6 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$arrInfo[0]['pendentes']?></h3>
              <p>Análises Pendentes</p>
            </div>
            <div class="icon">
              <i class="fa fa-clock-o"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=$arrInfo[0]['execucoes_mes']?></h3>
              <p>Análises Executadas no Mês</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
        <div class="col-lg-6 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo 0?></h3>
              <p>Tempo Médio de Análise</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        <!-- </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $intQuantidadeAcessos;?></h3>
              <p>Acessos do Dia</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
      </div>
      <div class="row">

          <div class="col-lg-6">
              <canvas id="myChart" width="400" height="200"></canvas>
          </div>


            <div class="col-lg-6 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed " id="cntExecute"
                      data-height="450" data-select-item-name="toolbar1"     data-pagination="true" data-page-list="[<?php echo CI_PAGINACAO?>]">
                        <thead>
                            <tr>
                                <th data-field="id" data-align="left" data-sortable="true" data-visible="false">#</th>
                                <th data-field="service_order_id" data-align="center" data-sortable="true" data-visible="true">Número OS</th>
                                <th data-field="analyst" data-align="left" data-sortable="true" data-visible="true">Analista</th>
                                <th data-field="client_id" data-align="left" data-sortable="true" data-visible="true">Cliente</th>
                                <th data-field="service_order_status" data-align="center" data-sortable="true" data-visible="true">Status</th>
                                <th data-field="service_order_deadline" data-align="center" data-sortable="true" data-visible="true">Prazo de Entrega</th>
                                <th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
                            </tr>
                        </thead>
                    </table>
                 </div>
            </div>
        </div>

    </section>
  </div>
<?php
    } elseif(UserHasPermissions($this, 'CLIENT_HOMESCREEN')){
?>



    <section class="content" id="divContainer">
      <div class="row" style="margin-top:25px">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$arrInfo[0]['pendentes']?></h3>
              <p>Análises Pendentes</p>
            </div>
            <div class="icon">
              <i class="fa fa-clock-o"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=$arrInfo[0]['execucoes_mes']?></h3>
              <p>Análises Executadas no Mês</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?=$arrInfo[0]['execucoes']?></h3>
              <p>Análises Executadas Acumuladas</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?=rand(1,5)?></h3>
              <p>Acessos do Dia</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="row">
          <div class="col-lg-12">
              <canvas id="myChart2" width="300" height="70"></canvas>
          </div>
      </div>
    </section>







<?php }  else { ?>
    <div class="content">
    <div class="row" style="margin-top:25px">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?=$arrInfo[0]['pendentes']?></h3>
            <p>Análises Pendentes</p>
          </div>
          <div class="icon">
            <i class="fa fa-clock-o"></i>
          </div>
          <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?=$arrInfo[0]['execucoes_mes']?></h3>
            <p>Análises Executadas no Mês</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?=$arrInfo[0]['execucoes']?></h3>
            <p>Análises Executadas Acumuladas</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?=rand(1,6)?></h3>
            <p>Acessos do Dia</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="javascript: void('<?php echo CI_SIGLA?>');" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <canvas id="myChart3" width="300" height="150"></canvas>
        </div>
        <div class="col-lg-6">
            <canvas id="myChart4" width="300" height="150"></canvas>
        </div>
    </div>



<?php } ?>
<?php
	$this->load->view('commons/rodape-logado');
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script>
var data = [
<?php
if ($arrServiceOrders != null){
	$intI = 0;
	foreach($arrServiceOrders as $l){
		$virgula = "";
		if( count($arrServiceOrders) - 1 > $intI) $virgula = ",";

		echo "{
			'id': '".$l['id']."',
            'service_order_id': '".$l['service_order_id']."',
            'analyst': '".$l['name']."',
            'client_id': '".$l['fantasy_name']."',
            'service_order_status': '".$l['service_order_status']."',
            'service_order_deadline': '".mudarFormatoData($l['service_order_deadline'])."'
		}$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntExecute').bootstrapTable({ data: data });
});

function editarConsulta(value, row, index){
    return [
        '<a href="<?=base_url('servicedetail_execute')?>/'+row.id+'" class="btn btn-success btn-sm">',
        '<i class="fa fa-play"></i>',
        '</a>'
    ].join('');
}
</script>
<script type="text/javascript">
<?php if(UserHasPermissions($this, 'ANALYST_HOMESCREEN')){ ?>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
type: 'bar',
data: {
    labels: ['<?=$arrExec[0]['semana']?>'],
    datasets: [{
        label: '# Atividades Executadas por Tempo',
        data: ['<?=$arrExec[0]['qtd']?>'],
        backgroundColor: [
            'rgba(8, 70, 153, 0.77)',
            'rgba(8, 70, 153, 0.77)',
            'rgba(8, 70, 153, 0.77)',
            'rgba(8, 70, 153, 0.77)',
            'rgba(8, 70, 153, 0.77)'
        ],
        borderColor: [
            'rgba(8, 70, 153, 1)',
            'rgba(8, 70, 153, 1)',
            'rgba(8, 70, 153, 1)',
            'rgba(8, 70, 153, 1)',
            'rgba(8, 70, 153, 1)'
        ],
        borderWidth: 1
    }]
},
options: {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
    }
}
});

<?php } elseif(UserHasPermissions($this, 'CLIENT_HOMESCREEN')){ ?>

    var ctx2 = document.getElementById("myChart2").getContext('2d');
    var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: ['<?=$arrExec[0]['semana']?>'],
        datasets: [{
            label: '# RElatórios Executados durante o tempo',
            data: ['<?=$arrExec[0]['qtd']?>'],
            backgroundColor: [
                'rgba(8, 70, 153, 0.77)',
                'rgba(8, 70, 153, 0.77)',
                'rgba(8, 70, 153, 0.77)',
                'rgba(8, 70, 153, 0.77)',
                'rgba(8, 70, 153, 0.77)'
            ],
            borderColor: [
                'rgba(8, 70, 153, 1)',
                'rgba(8, 70, 153, 1)',
                'rgba(8, 70, 153, 1)',
                'rgba(8, 70, 153, 1)',
                'rgba(8, 70, 153, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
    });

 <?php } else { ?>
     var ctx3 = document.getElementById("myChart3").getContext('2d');
     var myChart3 = new Chart(ctx3, {
     type: 'line',
     data: {
         labels: ['<?=$arrExec[0]['semana']?>'],
         datasets: [{
             label: '# Relatórios Executados durante o tempo',
             data: ['<?=$arrExec[0]['qtd']?>'],
             backgroundColor: [
                 'rgba(8, 70, 153, 0.77)',
                 'rgba(8, 70, 153, 0.77)',
                 'rgba(8, 70, 153, 0.77)',
                 'rgba(8, 70, 153, 0.77)',
                 'rgba(8, 70, 153, 0.77)'
             ],
             borderColor: [
                 'rgba(8, 70, 153, 1)',
                 'rgba(8, 70, 153, 1)',
                 'rgba(8, 70, 153, 1)',
                 'rgba(8, 70, 153, 1)',
                 'rgba(8, 70, 153, 1)'
             ],
             borderWidth: 1
         }]
     },
     options: {
         scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero:true
                 }
             }]
         }
     }
     });

     var ctx4 = document.getElementById("myChart4").getContext('2d');
     var myChart4 = new Chart(ctx4, {
     type: 'bar',
     data: {
         labels: ['<?=$arrStatus[0]['service_order_status']?>'],
         datasets: [{
             label: '# Relatórios Executados durante o tempo',
             data: [<?=$arrStatus[0]['qtd']?>],
             backgroundColor: [
                 'rgba(11, 153, 8, 0.77)',
                 'rgba(8, 70, 153, 0.77)',
                 'rgba(8, 140, 153, 0.77)',
                 'rgba(153, 8, 8, 0.77)',
                 'rgba(146, 153, 8, 0.77)'
             ],
             borderColor: [
                 'rgba(11, 153, 8, 1)',
                 'rgba(8, 70, 153, 1)',
                 'rgba(8, 140, 153, 1)',
                 'rgba(153, 8, 8, 1)',
                 'rgba(146, 153, 8, 1)'
             ],
             borderWidth: 1
         }]
     },
     options: {
         scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero:true
                 }
             }]
         }
     }
     });
 <?php }?>

</script>
