<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
					<form class="form-horizontal" id="frmFormulario" method="post" action="<?php echo base_url($this->router->routes['clients_save'])?>" enctype="multipart/form-data">
						<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
						<div class="box-body">
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="company_id">CNPJ</label>
							  <div class="col-md-2">
								<input type="text" name="company_id" id="company_id" autocomplete="off" value="<?php echo $arrDados[0]['company_id']?>" class="form-control maskCNPJ" required/>
							  </div>
							</div>
                            <div class="form-group form-group-sm">
                              <label class="col-md-2 control-label" for="fantasy_name">Nome Fantasia</label>
                              <div class="col-md-4">
                                <input type="text" name="fantasy_name" id="fantasy_name" autocomplete="off" value="<?php echo $arrDados[0]['fantasy_name']?>"  maxlength="255" class="form-control" required/>
                              </div>
                            </div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="company_name">Nome da Empresa</label>
							  <div class="col-md-4 has-feedback">
								  <input type="text" name="company_name" autocomplete="off" value="<?php echo $arrDados[0]['company_name']?>" id="company_name" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="telephone">Telefone</label>
							  <div class="col-md-2">
								<input type="text" name="telephone" id="telephone" autocomplete="off" value="<?php echo $arrDados[0]['telephone']?>"  class="form-control maskTelefone" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="celphone">Celular</label>
							  <div class="col-md-2">
								<input type="text" name="celphone" id="celphone" autocomplete="off" value="<?php echo $arrDados[0]['celphone']?>"  class="form-control maskCelular" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="address_number">Endereço</label>
							      <div class="col-md-4">
									<input type="text" name="address" value="<?php echo $arrDados[0]['address']?>" autocomplete="off" id="address" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-md-2 control-label" for="address_number">Número</label>
                                <div class="col-md-1">
  									<input type="text" name="address_number" value="<?php echo $arrDados[0]['address_number']?>" autocomplete="off" id="address_number" class="form-control" required/>
  								</div>
                                <label class="col-md-1 control-label" for="address_number">Complemento</label>
                                <div class="col-md-1">
    							     <input type="text" name="complement" autocomplete="off" value="<?php echo $arrDados[0]['complement']?>" id="complement" class="form-control"/>
                                </div>
                            </div>
                            </div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="general_information">Comentários Gerais</label>
							  <div class="col-md-4 has-feedback">
								  <textarea class="textarea" name="general_information" id="general_information" style="width:100%;height:100px;font-size:14px;line-height:18px;border:1px solid #dddddd;padding:10px;"><?php echo $arrDados[0]['general_information']?></textarea>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="status">Status</label>
							  <div class="checkbox col-md-2">
								<label>
									<input type="checkbox" <?php echo $checar?> name="status" id="status"/> <?php echo CI_LABEL_INATIVO?>
								</label>
							  </div>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
