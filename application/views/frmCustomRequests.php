<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
					<form class="form-horizontal" id="frmFormulario" method="post" action="<?php echo base_url($this->router->routes['custom_requests_save'])?>" enctype="multipart/form-data">
						<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
						<div class="box-body">
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="company_id">Solicitação da Relação</label>
							  <div class="col-md-4">
								<input type="text" name="request_name" id="request_name" autocomplete="off" value="<?php echo $arrDados[0]['request_name']?>" class="form-control" required/>
							  </div>
							</div>
                            <div class="form-group form-group-sm">
                              <label class="col-md-2 control-label" for="fantasy_name">CNPJ</label>
                              <div class="col-md-2">
                                <input type="text" name="company_id" id="company_id" autocomplete="off" value="<?php echo $arrDados[0]['company_id']?>"  maxlength="255" class="form-control maskCNPJ" required/>
                              </div>
                            </div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="company_name">Relações de Pesquisa</label>
							  <div class="col-md-2 has-feedback">
                               <input style="margin-right:5px"  type="checkbox" name="control_holding" autocomplete="off" value="<?php $arrDados[0]['control_holding']?>" id="control_holding" /> Cadeia de Controle/Holding
							  </div>
                              <div class="col-md-2 has-feedback">
                              <input style="margin-right:5px"  type="checkbox" name="partner_shareholder" autocomplete="off" value="<?php $arrDados[0]['partner_shareholder']?>" id="partner_shareholder" /> Sócios /Acionistas 
							  </div>
                              <div class="col-md-2 has-feedback">
                               <input style="margin-right:5px"  type="checkbox" name="consortium" autocomplete="off" value="<?php $arrDados[0]['consortium']?>" id="consortium"  /> Consórcios
							  </div>
                              <div class="col-md-2 has-feedback">
                               <input style="margin-right:5px" type="checkbox" name="adminstration_staff" autocomplete="off" value="<?php $arrDados[0]['administration_staff']?>" id="adminstration_staff"  /> Diretoria /Administração
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="email">E-mail</label>
							  <div class="col-md-2">
								<input type="text" name="email" id="email" autocomplete="off" value="<?php echo $arrDados[0]['email']?>"  class="form-control " required/>
							  </div>
							</div>
                           
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="general_information">Comentários Gerais</label>
							  <div class="col-md-4 has-feedback">
								  <textarea class="textarea" name="general_information" id="general_information" style="width:100%;height:100px;font-size:14px;line-height:18px;border:1px solid #dddddd;padding:10px;"><?php echo $arrDados[0]['general_information']?></textarea>
									<div class="file btn btn-success" style="overflow:hidden;position:relative;margin-top:10px">
									<i class="fa fa-paperclip"></i> Anexar arquivos<input type="file" style="position: absolute;font-size: 50px;opacity: 0;right: 0;top: 0;" name="file"/></div>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
