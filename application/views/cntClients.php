<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<div class="table-responsive">
						<table data-show-toggle='true' class="table table-striped table-hover table-condensed " id="cntConsulta"
						  data-height="450"   data-show-columns="true"
						  data-search="true"  data-select-item-name="toolbar1"
						  data-pagination="true" data-page-list="[<?php echo CI_PAGINACAO?>]">
							<thead>
								<tr>
									<th data-field="id" data-align="left" data-sortable="true" data-visible="false">ID.</th>
									<th data-field="company_name" data-align="left" data-sortable="true" data-visible="true">Company Name</th>
									<th data-field="company_id" data-align="left" data-sortable="true" data-visible="false">Company ID.</th>
									<th data-field="company_position" data-align="left" data-sortable="true" data-visible="false">Company Position</th>
									<th data-field="telephone" data-align="left" data-sortable="true" data-visible="true">Telephone</th>
									<th data-field="celphone" data-align="left" data-sortable="true" data-visible="true">Celphone</th>
									<th data-field="fantasy_name" data-align="left" data-sortable="true" data-visible="false">Fantasy Name</th>
									<th data-field="address" data-align="center" data-sortable="true" data-visible="false">Address</th>
									<th data-field="address_number" data-align="center" data-sortable="true" data-visible="false">Address Number</th>
									<th data-field="complement" data-align="center" data-sortable="true" data-visible="false">Complement</th>
									<th data-field="general_information" data-align="center" data-sortable="true" data-visible="false">General Information</th>
									<th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
									<th data-field="excluirConsulta"data-align="center" data-switchable="false" data-formatter="excluirConsulta"></th>
								</tr>
							</thead>
						</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
var data = [
<?php
if ($arrDados != null){
	$intI = 0;
	foreach($arrDados as $l){
		$virgula = "";
		if( count($arrDados) - 1 > $intI) $virgula = ",";

		echo "{
			'id': '".$l['id']."',
			'company_name': '".$l['company_name']."',
			'company_id': '".$l['company_id']."',
			'company_position': '".$l['company_position']."',
			'telephone': '".$l['telephone']."',
			'celphone': '".$l['celphone']."',
			'fantasy_name': '".$l['fantasy_name']."',
			'address': '".$l['address']."',
			'address_number': '".$l['address_number']."',
			'general_information': '".$l['general_information']."'
		}$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({ data: data });
});

function excluirConsulta(value, row, index){
    return [
        '<a href="<?=base_url('clients_delete')?>/'+row.id+'" class="btn btn-danger btn-sm">',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}

function editarConsulta(value, row, index){
    return [
        '<a href="<?php echo CI_ACAO_EDITAR?>/'+row.id+'" class="btn btn-primary btn-sm" title="<?php echo CI_TITLE_EDITAR?>">',
        '<span class="glyphicon glyphicon-pencil"></span>',
        '</a>'
    ].join('');
}
</script>
