<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<div class="table-responsive">
						<table data-show-toggle='true' class="table table-striped table-hover table-condensed " id="cntConsulta"  data-height="450" data-show-columns="true"  data-search="true"  data-select-item-name="toolbar1" data-pagination="true" data-page-list="[<?php echo CI_PAGINACAO?>]">
							<thead>
								<tr>
									<th data-field="id" data-align="left" data-sortable="true" data-visible="false">ID</th>
									<th data-field="report_name" data-align="left" data-sortable="true" data-visible="true">Relatório</th>
									<th data-field="fantasy_name" data-align="left" data-sortable="true" data-visible="true">Cliente</th>
									<th data-field="nickname" data-align="left" data-sortable="true" data-visible="true">Feito por</th>
									<th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
									<th data-field="excluirConsulta"data-align="center" data-switchable="false" data-formatter="excluirConsulta"></th>
								</tr>
							</thead>
						</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
var data = [
<?php
if ($arrDados != null){
	$intI = 0;
	foreach($arrDados as $l){
		$virgula = "";
		if( count($arrDados) - 1 > $intI) $virgula = ",";

		echo "{
    			'id': '".$l['id']."',
                'report_name': '".$l['report_name']."',
    			'fantasy_name': '".$l['fantasy_name']."',
    			'nickname': '".$l['nickname']."'
		    }$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({data:data});
});

function excluirConsulta(value, row, index){
    return [
        '<a href="<?=base_url('report_designer_delete')?>/'+row.id+'" class="btn btn-danger btn-sm">',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}

function editarConsulta(value, row, index){
    return [
        '<a href="<?=base_url('report_designer_edit')?>/'+row.id+'" class="btn btn-primary btn-sm">',
        '<span class="glyphicon glyphicon-pencil"></span>',
        '</a>'
    ].join('');
}
</script>
