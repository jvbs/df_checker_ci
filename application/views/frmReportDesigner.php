<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
    <div class="container-fluid">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <form class="form-horizontal" id="frmFormulario" method="post" action="<?=base_url($this->router->routes['report_designer_save'])?>" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group form-group-sm" style="padding:0px 15px">
                                <label class="control-label" for="report-name">Nome do Relatório</label>
                                <input type="text" name="report_name" id="report_name" value="<?=$arrDados[0]['report_name']?>" autocomplete="off"  maxlength="255" class="form-control" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group form-group-sm" style="padding:0px 15px">
                                <label class="control-label" for="USU_Nome">Cliente</label>
                                <?php echo select_clients($arrClients, CI_SELECT_OBRIGATORIO, $arrDados[0]['client_id']);?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table data-show-toggle='false' class="table table-striped table-hover table-condensed" id="cntConsulta" data-height="450" data-show-columns="true" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-page-list="[200]">
                                    <thead>
                                        <tr>
                                            <th data-field="id" data-align="center" data-sortable="true" data-visible="true">#</th>
                                            <th data-field="query_mode" data-align="center" data-sortable="true" data-visible="true">Modo</th>
                                            <th data-field="query_source" data-align="left" data-sortable="true" data-visible="true">Fonte</th>
                                            <th data-field="query_classify" data-align="left" data-sortable="true" data-visible="true">Classificação</th>
                                            <th data-field="query_type" data-align="left" data-sortable="true" data-visible="true">Tipo</th>
                                            <th data-field="query_description" data-align="center" data-sortable="true" data-visible="false">Descrição</th>
                                            <th data-field="query_value" data-align="center" data-sortable="true" data-visible="true">Valor</th>
                                        </tr>
                                    </thead>
                                </table>
                             </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" class="btn btn-default" id="btnCancelar">
                        <i class="glyphicon glyphicon-remove-circle"></i> <?=CI_LABEL_BTN_CANCELAR?>
                    </button>
                    <button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?=CI_LABEL_BTN_CADASTRAR?>
                        <i class="glyphicon glyphicon-ok-circle"></i>
                    </button>
                </div>
            </form>
		</div>
    </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
<?php $queries = explode(',', $arrDados[0]['queries']); ?>
var data = [
<?php

if (isset($arrQueryList)){
	$intI = 0;
	foreach($arrQueryList as $l){
        if(in_array($l['id'], $queries)){
            $checkme = " checked";
        } else {
            $checkme = "";
        }
		$virgula = "";
		if(count($arrQueryList) - 1 > $intI) $virgula = ",";
    		echo "{
        		'id': '<input type=checkbox name=checklist[] value=".$l['id']."{$checkme}>',
        		'query_mode': '".$l['query_mode']."',
        		'query_source': '".$l['query_source']."',
        		'query_classify': '".$l['query_classify']."',
        		'query_type': '".$l['query_type']."',
        		'query_description': '".$l['query_description']."',
        		'query_value': '".formatarMoeda($l['query_value'])."'
    		    }$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({data: data});
});
</script>
