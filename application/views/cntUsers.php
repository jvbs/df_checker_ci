<?php $this->load->view('commons/cabecalho-logado');?>
<input type="hidden" name="FRM_ID2" id="FRM_ID2" value="<?php echo $FRM_ID;?>"/>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<div class="table-responsive">
						<table data-show-toggle='true' class="table table-striped table-hover table-condensed " id="cntConsulta"
						  data-height="450"   data-show-columns="true"
						  data-search="true"  data-select-item-name="toolbar1"
						  data-pagination="true" data-page-list="[<?php echo CI_PAGINACAO?>]">
							<thead>
								<tr>
									<th data-field="id" data-align="left" data-sortable="true" data-visible="true">#</th>
									<th data-field="name" data-align="left" data-sortable="true" data-visible="true">Nome</th>
									<th data-field="email" data-align="left" data-sortable="true" data-visible="true">E-mail</th>
									<th data-field="nickname" data-align="left" data-sortable="true" data-visible="false">Apelido</th>
									<th data-field="birthdate" data-align="center" data-sortable="true" data-visible="true">Data de Nascimento</th>
									<th data-field="created_at" data-align="center" data-sortable="true" data-visible="false">Criado em</th>
									<th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
									<th data-field="excluirConsulta"data-align="center" data-switchable="false" data-formatter="excluirConsulta"></th>
								</tr>
							</thead>
						</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
var data = [
<?php
if ($arrDados != null){
	$intI = 0;
	foreach($arrDados as $l){
		$virgula = "";
		if( count($arrDados) - 1 > $intI) $virgula = ",";

		echo "{
			'id': '".$l['id']."',
			'name': '".$l['name']."',
			'email': '".$l['email']."',
			'nickname': '".$l['nickname']."',
			'birthdate': '".mudarFormatoData($l['birthdate'])."',
			'created_at': '".mudarFormatoData($l['created_at'])."'
		}$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({ data: data });
});

function excluirConsulta(value, row, index){
    console.log(row.id);
    return [
        "<a href=\"javascript: void('<?php echo CI_SIGLA?>');\" onClick=\"$('#hddExcluir').val('<?php echo base_url('users_delete')?>/"+row.id+"');$('#descricaoExcluir').html('<?php echo CI_LABEL_CONFIRMAR_EXCLUSAO?>');\" data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger btn-sm' title='<?php echo CI_TITLE_EXCLUIR?>'>",
        "<span class='glyphicon glyphicon-trash'></span>",
        "</a>"
    ].join('');
}

function editarConsulta(value, row, index){
    return [
        '<a href="<?php echo CI_ACAO_EDITAR?>/'+row.id+'" class="btn btn-primary btn-sm" title="<?php echo CI_TITLE_EDITAR?>">',
        '<span class="glyphicon glyphicon-pencil"></span>',
        '</a>'
    ].join('');
}
</script>
