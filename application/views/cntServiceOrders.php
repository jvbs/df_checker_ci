<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<div class="table-responsive">
						<table data-show-toggle='true' class="table table-striped table-hover table-condensed " id="cntConsulta"
						  data-height="450"   data-show-columns="true"
						  data-search="true"  data-select-item-name="toolbar1"
						  data-pagination="true" data-page-list="[<?php echo CI_PAGINACAO?>]">
							<thead>
								<tr>
                                    <th data-field="ID" data-align="left" data-sortable="true" data-visible="false">ID</th>
                                    <th data-field="service_order_id" data-align="left" data-sortable="true" data-visible="true">Número OS</th>
									<th data-field="client" data-align="left" data-sortable="true" data-visible="true">Cliente</th>
									<th data-field="analyst" data-align="left" data-sortable="true" data-visible="true">Analista</th>
                                    <th data-field="service_order_status" data-align="left" data-sortable="true" data-visible="true">Status</th>
									<th data-field="service_order_date" data-align="left" data-sortable="true" data-visible="true">Data Cadastro</th>
									<th data-field="service_order_deadline" data-align="left" data-sortable="true" data-visible="true">Prazo de Entrega</th>
									<th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
									<th data-field="excluirConsulta"data-align="center" data-switchable="false" data-formatter="excluirConsulta"></th>
								</tr>
							</thead>
						</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
var data = [
<?php
if ($arrDados != null){
	$intI = 0;
	foreach($arrDados as $l){
		$virgula = "";
		if(count($arrDados) - 1 > $intI) $virgula = ",";

		echo "{
            'id': '".$l['id']."',
            'service_order_id': '".$l['service_order_id']."',
            'service_order_status': '".$l['service_order_status']."',
			'client': '".$l['fantasy_name']."',
			'analyst': '".$l['name']."',
			'service_order_date': '".mudarFormatoData($l['service_order_date'])."',
			'service_order_deadline': '".mudarFormatoData($l['service_order_deadline'])."'
		}$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({ data: data });
});

function excluirConsulta(value, row, index){
    return [
        '<a href="<?=base_url('serviceorders_delete')?>/'+row.id+'/'+row.service_order_id+'" class="btn btn-danger btn-sm">',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}

function editarConsulta(value, row, index){
    return [
        '<a href="<?php echo CI_ACAO_EDITAR?>/'+row.id+'" class="btn btn-primary btn-sm" title="<?php echo CI_TITLE_EDITAR?>">',
        '<span class="glyphicon glyphicon-pencil"></span>',
        '</a>'
    ].join('');
}
</script>
