<?php $this->load->view('commons/cabecalho');?>


<style>
body {background-color:#384149}
</style>
<?php if ($this->session->userdata('error_crud')): ?>
	<div id="div-error" class="alert alert-danger"><?php echo $this->session->userdata('error_crud'); ?></div>
<?php $this->session->unset_userdata('error_crud'); endif; ?>
    <div class="d-flex flex-row justify-content-center px-4" style="margin-top:9vh;" >
        <div class="card center-block" style="width:38rem;background-color:#fff; padding: 20px">
            <div class="card-header py-3  d-flex justify-content-between align-items-center" style="border-bottom: 1px solid #eee;">
                <img src="<?=base_url()?>assets/images/df_checker_1.svg" style="width:100%" alt="Logo DF Checker">
            </div>
            <div class="card-body">
                <form class="" method="POST" action="<?=base_url($this->router->routes['users_login'])?>">
                    <div class="form-group mb-4 mt-2">
                        <label>CNPJ <span style="color:red">*</span></label>
                        <input type="text" autocomplete="off" class="form-control maskCNPJ" id="cnpj" name="cnpj" placeholder="">
                    </div>
                    <!-- <div class="form-group mb-4 mt-2">
                        <label>CPF <span style="color:red">*</span></label>
                        <input type="text" autocomplete="off" class="form-control" id="cpf" name="cpf" placeholder="">
                    </div> -->
                    <div class="form-group mb-4">
                        <label>Senha <span style="color:red">*</span></label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="">
                    </div>
                    <!--<div class="form-check mb-2 py-2">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Keep Me Logged In</span>
                        </label>
                    </div>-->
                    <div class="py-1">
                        <button type="submit" class="btn btn-info btn-block">Login</button>
                    </div>
                <!--<div class="py-0 mt-2 d-flex justify-content-around">
                    <button class="btn btn-flat-info"><i class="fa fa-twitter"></i></button>
                    <button class="btn btn-flat-danger"><i class="fa fa-google"></i></button>
                    <button class="btn btn-flat-info"><i class="fa fa-facebook"></i></button>
                    <button class="btn btn-flat-warning"><i class="fa fa-stack-overflow"></i></button>
                  </div> -->
                </form>
<!--                 <div class="py-2 px-0">
                    <p class="mb-0">
                        <button class=" btn btn-link pl-1">Esqueceu sua senha?</button>
                    </p>
                </div> -->
            </div>
        <!--<div class="card-footer">
            <button class=" btn btn-link pl-1">Don't have an account? Signup Now!</button>
        </div> -->
        </div>
    </div>
</div>
<?php $this->load->view('commons/rodape');?>
<script>
	$(document).ready(function (){
		$('#txtLogin').focus();
		$('#div-error').fadeOut(parseInt(<?php echo CI_FADEOUT?>));
		$('[data-toggle="tooltip"]').tooltip(); //Ativar tooltip aloouuuuu!!!
	});
</script>
