<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<form class="form-horizontal" id="frmFormulario" method="post" action="<?php echo base_url($this->router->routes['querylist_save'])?>" enctype="multipart/form-data">
						<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
						<div class="box-body">
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="table_type">Tipo de Tabela</label>
							  <div class="col-md-4">
                                  <select class="form-control" name="table_type">
                                      <option value="FONTES">FONTES</option>
                                      <option value="TRIBUNAIS">TRIBUNAIS</option>
                                      <option value="DIARIO OFICIAL">DIARIO OFICIAL</option>
                                  </select>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_mode">Modo de Consulta</label>
							  <div class="col-md-4">
								  <input type="text" name="query_mode" autocomplete="off" value="<?php echo $arrDados[0]['query_mode']?>" id="query_mode" placeholder="" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_source">Fonte de Consulta</label>
							  <div class="col-md-4">
								  <input type="text" name="query_source" autocomplete="off" value="<?php echo $arrDados[0]['query_source']?>" id="query_source" placeholder="" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_classify">Classificação</label>
							  <div class="col-md-4">
							      <select name="query_classify" id="query_classify" placeholder="Query Classify" class="form-control" required/>
                                      <option value="DADOS ABERTOS DO GOVERNO">DADOS ABERTOS DO GOVERNO</option>
                                      <option value="ENTIDADES SETORIAIS">ENTIDADES SETORIAIS</option>
                                      <option value="FINANCEIRO/ECONÔMICO">FINANCEIRO/ECONÔMICO</option>
                                      <option value="INFORMAÇÕES CADASTRAIS">INFORMAÇÕES CADASTRAIS</option>
                                      <option value="RELAÇÕES POLÍTICAS">RELAÇÕES POLÍTICAS</option>
                                      <option value="RISCOS AMBIENTAIS">RISCOS AMBIENTAIS</option>
                                      <option value="RISCOS FINANCEIROS">RISCOS FINANCEIROS</option>
                                      <option value="RISCOS INTERNACIONAIS">RISCOS INTERNACIONAIS</option>
                                      <option value="RISCOS LEGAIS E CRIMINAIS">RISCOS LEGAIS E CRIMINAIS</option>
                                      <option value="RISCOS REPUTACIONAIS">RISCOS REPUTACIONAIS</option>
                                  </select>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_type">Tipo de Consulta</label>
							  <div class="col-md-4">
								  <select name="query_type" id="query_type" class="form-control" required/>
                                        <option value="PÚBLICA">PÚBLICA</option>
                                        <option value="PAGA">PAGA</option>
                                        <option value="INTERNA">INTERNA</option>
                                  </select>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_description">Descrição</label>
							  <div class="col-md-4">
								  <textarea rows="6" name="query_description" id="query_description" class="form-control" required/></textarea>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_value">Valor</label>
							  <div class="col-md-4">
								  <input type="text" name="query_value" autocomplete="off" value="<?php echo $arrDados[0]['query_value']?>" id="query_value" class="form-control maskMoney" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_entries">Entrada</label>
							  <div class="col-md-4">
								  <input type="text" name="query_entries" autocomplete="off" value="<?php echo $arrDados[0]['query_entries']?>" id="query_entries" placeholder="Query Entries" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="query_profile">Perfil</label>
							  <div class="col-md-4">
								  <input type="text" name="query_profile" autocomplete="off" value="<?php echo $arrDados[0]['query_profile']?>" id="query_profile" placeholder="Query Profile" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="link_status">Link Status</label>
							  <div class="col-md-4">
								  <input type="text" name="link_status" autocomplete="off" value="<?php echo $arrDados[0]['link_status']?>" id="link_status" placeholder="Link Status" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="link">Link</label>
							  <div class="col-md-4">
								  <input type="text" name="link" autocomplete="off" value="<?php echo $arrDados[0]['link']?>" id="link" placeholder="Link" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="status">Status</label>
							  <div class="checkbox col-md-2">
								<label>
									<input type="checkbox" <?php echo $checar?> name="status" id="status"/> <?php echo CI_LABEL_INATIVO?>
								</label>
							  </div>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
