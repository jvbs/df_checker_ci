<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <form class="form-horizontal" id="frmFormulario" method="post" action="<?=base_url($this->router->routes['direct_requests_save'])?>" enctype="multipart/form-data">
				<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
				<div class="box-body">
                    <div class="container-fluid">


                    <?php if(!isset($arrDados[0]['id'])){ ?>
                        <div class="row" >
                            <div class="col-md-12" style="border-bottom:1px solid lightgrey">
                                <a href="#" id="addSearch" style="margin: 0px 0px 20px -15px" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar pesquisa (<span id="counter">1</span>/10)</a>

                            </div>
                            <div class="start-search" style="margin-left:15px; ">
                                <div class="search-block">
                                    <div class="this-block" style="margin-bottom: 25px">


                                    <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group form-group-sm">
                                             <select class="form-control" name="cpf-cnpj[]" style="margin-top:23px">
                                                 <option value="CPF">CPF</option>
                                                 <option value="CNPJ">CNPJ</option>
                                             </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-sm">
                                            <label class="control-label" for="target_company_id">CPF/CNPJ</label>
                                            <input type="text" name="target_company_id[]" id="target_company_id" autocomplete="off" value="<?=$arrDados[0]['target_company_id']?>" placeholder="" maxlength="14" class="form-control" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Pesquisado</label>
                						        <input type="text" name="target_name[]" id="target_name" autocomplete="off" value="<?php echo $arrDados[0]['target_name']?>" maxlength="255" class="form-control" required/>
                						</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Relatório</label>
                						    <?php echo select_report($arrReports, CI_SELECT_OBRIGATORIO, $arrDados[0]['id']);?>
                						</div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <div class="row" style="border-top:1px solid lightgrey">
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Valor Total</label>
                                <input type="text" class="form-control maskMoney" value="<?=$arrDados[0]['full_value']?>" id="full-value" name="full-value" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">E-mail do Solicitante</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['soliciter_email']?>" id="soliciter-email" name="soliciter-email" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Prazo</label>
                                <select class="form-control" name="deadline_status">
                                    <option value="">-- SELECIONE --</option>
                                    <option value="Extremamente Urgente">Extremamente Urgente</option>
                                    <option value="Urgente">Urgente</option>
                                    <option value="Padrão">Padrão</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    </div>
					<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							</button>
						</div>
			</form>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>

$("#addSearch").click(function(e){
    e.preventDefault();
    var limite = 10;
    var contador = parseFloat($("#counter").text());
    if(limite > contador){
        var get = $(".search-block").html();
        $(".start-search").append(get);
        $("#counter").text(contador+1);
    }



});


</script>
