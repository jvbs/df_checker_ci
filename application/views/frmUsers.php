<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row"> 
	<div class="col-sm-12">	
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<form class="form-horizontal" id="frmFormulario" method="post" action="<?php echo base_url($this->router->routes['users_save'])?>" enctype="multipart/form-data">
						<input type="hidden" id="hddConsultarDados" value="<?php echo base_url('users_check_email')?>"/>
						<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
						<div class="box-body">							
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="PER_ID">User Level :</label>  
							  <div class="col-md-4">
								<?php echo select_user_level(CI_SELECT_OBRIGATORIO, $arrDados[0]['user_level']);?>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="name">Name :</label>  
							  <div class="col-md-4">
								<input type="text" name="name" id="name" autocomplete="off" value="<?php echo $arrDados[0]['name']?>" placeholder="Name" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="company_id">Company ID :</label>  
							  <div class="col-md-4">
								<input type="text" name="company_id" id="company_id" autocomplete="off" value="<?php echo $arrDados[0]['company_id']?>" placeholder="Company ID" class="form-control" required/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="nickname">Nickname :</label>  
							  <div class="col-md-4">
								<input type="text" name="nickname" id="nickname" autocomplete="off" value="<?php echo $arrDados[0]['nickname']?>" placeholder="Nickname" class="form-control" required/>
							  </div>
							</div>							
							<div class="form-group form-group-sm" id="grp-mail">
							  <label class="col-md-2 control-label" for="email">E-mail :</label>
							  <div class="col-md-4 has-feedback">
								  <input type="email" name="email" autocomplete="off" value="<?php echo $arrDados[0]['email']?>" id="email" placeholder="E-mail" class="form-control" required/>
								  <span id="spn-mail" class="glyphicon glyphicon-envelope form-control-feedback"></span>
							  </div>
							</div>
							<?php if (!isset($arrDados[0]['id'])){ ?>
							<div class="form-group form-group-sm" id="grp-pass">
							  <label class="col-md-2 control-label">Password :</label>
							  <div class="col-md-4 has-feedback">
								  <input type="password" name="password" autocomplete="off" id="password" onBlur="checarSenhas(this.value, $('#password2').val());" placeholder="Password" class="form-password form-control noTrim" required/>
								  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
							  </div>
							</div>
							<div class="form-group form-group-sm" id="grp-pass2">
							  <label class="col-md-2 control-label">Confirm Password :</label>
							  <div class="col-md-4 has-feedback">
								  <input type="password" name="password2" autocomplete="off" id="password2" placeholder="Confirm Password" onBlur="checarSenhas($('#password').val(), this.value);" class="form-password form-control noTrim" required/>
								  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
							  </div>
							</div>
							<?php } ?>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label">Picture :</label>
							  <div class="col-md-4">
								  <?php
									if (isset($arrDados[0]['user_profile_pic'])){
										if (is_url_exist(CI_DIRETORIO_USUARIOS.$arrDados[0]['user_profile_pic'])){
											echo "
											<input type='hidden' name='USU_ImagemAnterior' id='USU_ImagemAnterior' value='".$arrDados[0]['user_profile_pic']."'/>
											<img class='pop' style='cursor:pointer;' id='exibirImagem' src='".CI_DIRETORIO_USUARIOS.$arrDados[0]['user_profile_pic']."' title='".$arrDados[0]['name']."' width='50px' height='50px'><br>
											<input type='checkbox' name='USU_ImagemCheck' id='USU_ImagemCheck'/> ".CI_LABEL_REMOVER_IMAGEM;
										}
									}
								  ?>
								  <input type="file" name="USU_Imagem" id="USU_Imagem" class="form-control"/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label">Birthdate :</label>
							  <div class="col-md-4 input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" name="birthdate" value="<?php echo $arrDados[0]['birthdate']?>" autocomplete="off" id="birthdate" placeholder="Birthdate" class="form-control hasDatepicker"/>
							  </div>
							</div>
							<div class="form-group form-group-sm">
							  <label class="col-md-2 control-label" for="USU_Status">Status</label>
							  <div class="checkbox col-md-2">
								<label>
									<input type="checkbox" <?php echo $checar?> name="USU_Status" id="USU_Status"/> <?php echo CI_LABEL_INATIVO?>
								</label>
							  </div>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" disabled="disabled" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i> 
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
	$(document).ready(function (){
		<?php if (!empty($arrDados[0]['id'])){ ?>
			console.log('1111');
			$('#email').trigger('keyup');
		<?php } ?>
	});
</script>