<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<div class="table-responsive">
						<table data-show-toggle='true' class="table table-striped table-hover table-condensed" id="cntConsulta"
						  data-height="450" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-list="[<?=CI_PAGINACAO?>]">
							<thead>
								<tr>
                                    <th data-field="state" data-align="left" data-visible="false" data-checkbox="true">#</th>
                                    <th data-field="id" data-align="center" data-sortable="true" data-visible="true">#</th>
									<th data-field="table_type" data-align="left" data-sortable="true" data-visible="false">Table Type</th>
									<th data-field="query_mode" data-align="left" data-sortable="true" data-visible="true">Query Mode</th>
									<th data-field="query_source" data-align="left" data-sortable="true" data-visible="true">Query Source</th>
									<th data-field="query_classify" data-align="left" data-sortable="true" data-visible="false">Query Classify</th>
									<th data-field="query_type" data-align="left" data-sortable="true" data-visible="true">Query Type</th>
									<th data-field="query_description" data-align="center" data-sortable="true" data-visible="false">Query Description</th>
									<th data-field="query_value" data-align="center" data-sortable="true" data-visible="false">Query Value</th>
									<th data-field="query_entries" data-align="center" data-sortable="true" data-visible="false">Query Entries</th>
									<th data-field="query_profile" data-align="center" data-sortable="true" data-visible="false">Query Profile</th>
									<th data-field="editarConsulta"data-align="center" data-switchable="false" data-formatter="editarConsulta"></th>
									<th data-field="excluirConsulta"data-align="center" data-switchable="false" data-formatter="excluirConsulta"></th>
								</tr>
							</thead>
						</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>
var data = [
<?php
if ($arrDados != null){
	$intI = 0;
	foreach($arrDados as $l){
		$virgula = "";
		if(count($arrDados) - 1 > $intI) $virgula = ",";

		echo "{
			'id': '".$l['id']."',
			'table_type': '".$l['table_type']."',
			'query_mode': '".$l['query_mode']."',
			'query_source': '".$l['query_source']."',
			'query_classify': '".$l['query_classify']."',
			'query_type': '".$l['query_type']."',
			'query_description': '".$l['query_description']."',
			'query_value': '".formatarMoeda($l['query_value'])."',
			'query_entries': '".$l['query_entries']."',
			'query_profile': '".$l['query_profile']."'
		}$virgula";
		$intI++;
	}
}
?>
];

$(function (){
    $('#cntConsulta').bootstrapTable({ data: data });
});

function excluirConsulta(value, row, index){
    return [
        "<a href=\"javascript: void('<?php echo CI_SIGLA?>');\" onClick=\"$('#hddExcluir').val('<?php echo base_url('querylist_delete')?>/"+row.id+"');$('#descricaoExcluir').html('<?php echo CI_LABEL_CONFIRMAR_EXCLUSAO?>');\" data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger btn-sm' title='<?php echo CI_TITLE_EXCLUIR?>'>",
        "<span class='glyphicon glyphicon-trash'></span>",
        "</a>"
    ].join('');
}

function editarConsulta(value, row, index){
    return [
        '<a href="<?php echo base_url('querylist_edit')?>/'+row.id+'" class="btn btn-primary btn-sm" title="<?php echo C_TITLE_EDITAR?>">',
        '<span class="glyphicon glyphicon-pencil"></span>',
        '</a>'
    ].join('');
}
</script>
