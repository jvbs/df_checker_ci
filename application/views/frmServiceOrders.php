<?php $this->load->view('commons/cabecalho-logado');?>
<section class="content">
 <div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->userdata('success_crud')): ?>
			<div id="div-success-view" class="alert alert-success"><?php echo $this->session->userdata('success_crud')?></div>
		<?php $this->session->unset_userdata('success_crud'); endif;?>
		<?php if ($this->session->userdata('danger_crud')): ?>
			<div id="div-danger-view" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
		<?php $this->session->unset_userdata('danger_crud'); endif; ?>
        <div id="page-wrapper">
            <form class="form-horizontal" id="frmFormulario" method="post" action="<?=base_url($this->router->routes['serviceorders_save'])?>" enctype="multipart/form-data">
				<input type="hidden" name="id" id="id" value="<?php echo $arrDados[0]['id']?>"/>
				<div class="box-body">
                    <div class="container-fluid">
                        <?php if(isset($arrDados[0]['id'])){ ?>
                            <div class="col-md-2">
                                <div class="form-group form-group-sm" style="margin-right:5px">
                                    <label class="control-label" for="USU_Nome">Número da OS</label>
                                    <input type="text" name="service_order_id" value="<?=$arrDados[0]['service_order_id']?>" autocomplete="off" id="service_order_id" placeholder="" class="form-control" readonly="readonly">
                                </div>
                            </div>
                        <?php } ?>

                            <div class="col-md-3">
                                <div class="form-group form-group-sm" style="margin-right:5px">
    							  <label class="control-label" for="USU_Nome">Cliente</label>
    								<?php echo select_clients($arrClients, CI_SELECT_OBRIGATORIO, $arrDados[0]['client_id']);?>
    							</div>
                            </div>

                            <div class="col-md-3">
    							<div class="form-group form-group-sm" style="margin-right:5px">
    							  <label class="control-label" for="analyst">Analista</label>
    								<?php echo select_analyst($arrAnalyst, CI_SELECT_OBRIGATORIO, $arrDados[0]['analyst']);?>
    							</div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group form-group-sm" style="margin-right:5px">
                                    <label class="control-label">Status da OS</label>
                                    <select name="status-os" class="form-control" id="status-os" required>
                                        <option value="">-- SELECIONE --</option>
                                        <option value="Aprovada">Aprovada</option>
                                        <option value="Aguardando execução">Aguardando execução</option>
                                        <option value="Em atraso">Em atraso</option>
                                        <option value="Em execução">Em execução</option>
                                        <option value="Finalizada">Finalizada</option>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($arrDados[0]['id'])){ ?>
                            <div class="col-md-2">
    							<div class="form-group form-group-sm" style="margin-right:5px">
    							    <label class="control-label">Data de Cadastro</label>
    								<input type="text" name="service_order_date" value="<?=mudarFormatoData($arrDados[0]['service_order_date'])?>" autocomplete="off" id="service_order_date" placeholder="" class="form-control" readonly="readonly">
    							</div>
                            </div>
                        <?php } ?>
                            <div class="col-md-2">
    							<div class="form-group form-group-sm" style="margin-right:5px">
    							     <label class="control-label">Prazo de Entrega</label>
    								  <input type="text" name="service_order_deadline" value="<?=mudarFormatoData($arrDados[0]['service_order_deadline'])?>" autocomplete="off" id="deadline" placeholder="" class="form-control hasDatepicker" required>
    							</div>
                            </div>
                    <?php if(!isset($arrDados[0]['id'])){ ?>
                        <div class="row" >
                            <div class="col-md-12" style="border-bottom:1px solid lightgrey">
                                <a href="#" id="addSearch" style="margin: 35px 0px 20px -15px" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar pesquisa (<span id="counter">1</span>/10)</a>

                            </div>
                            <div class="start-search" style="margin-left:15px; ">
                                <div class="search-block">
                                    <div class="this-block" style="margin-bottom: 25px">


                                    <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group form-group-sm">
                                             <select class="form-control" name="cpf-cnpj[]" style="margin-top:23px">
                                                 <option value="CPF">CPF</option>
                                                 <option value="CNPJ">CNPJ</option>
                                             </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-sm">
                                            <label class="control-label" for="target_company_id">CPF/CNPJ</label>
                                            <input type="text" name="target_company_id[]" id="target_company_id" autocomplete="off" value="<?=$arrDados[0]['target_company_id']?>" placeholder="" maxlength="14" class="form-control" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Pesquisado</label>
                						        <input type="text" name="target_name[]" id="target_name" autocomplete="off" value="<?php echo $arrDados[0]['target_name']?>" maxlength="255" class="form-control" required/>
                						</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-sm">
                					        <label class="control-label" for="target_name">Relatório</label>
                						    <?php echo select_report($arrReports, CI_SELECT_OBRIGATORIO, $arrDados[0]['id']);?>
                						</div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <div class="row" style="border-top:1px solid lightgrey">
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Valor Unitário</label>
                                <input type="text" class="form-control maskMoney" value="<?=$arrDados[0]['unit_value']?>" id="unit-value" name="unit-value" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Valor Total</label>
                                <input type="text" class="form-control maskMoney" value="<?=$arrDados[0]['full_value']?>" id="full-value" name="full-value" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Desconto (%)</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['discount']?>" id="discount" maxlength="3" name="discount" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Formas de Pagamento</label>
                                <select class="form-control" name="payment-methods" id="payment-methods" required>
                                    <option value="">---Selecione---</option>
                                    <option value="Boleto">Boleto</option>
                                    <option value="Cartão de Crédito">Cartão de Crédito</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Número da NF</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['nf_number']?>" id="nf-number" name="nf-number" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Data do Faturamento</label>
                                <input type="text" class="form-control hasDatepicker" value="<?=mudarFormatoData($arrDados[0]['fat_date'])?>" id="fat-date" name="fat-date" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Nome do Solicitante</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['soliciter_name']?>" id="soliciter-name" name="soliciter-name" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">E-mail do Solicitante</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['soliciter_email']?>" id="soliciter-email" name="soliciter-email" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Telefone</label>
                                <input type="text" class="form-control" value="<?=$arrDados[0]['soliciter_telephone']?>" id="soliciter-telephone" name="soliciter-telephone" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group form-group-sm" style="margin-right:5px">
                                <label class="control-label">Comentários Gerais</label>
                                <textarea class="form-control" name="general-comments" id="general-comments" cols="30" rows="10" required><?=$arrDados[0]['general_comments']?></textarea>
                            </div>
                        </div>
                    </div>




                    </div>
					<div class="box-footer">
							<button type="button" class="btn btn-default" id="btnCancelar">
								<i class="glyphicon glyphicon-remove-circle"></i> <?php echo CI_LABEL_BTN_CANCELAR?>
							</button>
							<button type="submit" id="btnSalvar" class="btn btn-info pull-right"><?php echo CI_LABEL_BTN_CADASTRAR?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							</button>
						</div>
			</form>
		</div>
	</div>
 </div>
</section>
<?php $this->load->view('commons/rodape-logado');?>
<script>

$("#addSearch").click(function(e){
    e.preventDefault();
    var limite = 10;
    var contador = parseFloat($("#counter").text());
    if(limite > contador){
        var get = $(".search-block").html();
        $(".start-search").append(get);
        $("#counter").text(contador+1);
    }



});


</script>
