	  <footer class="box-footer">
		<div class="pull-right hidden-xs"><?php echo CI_VERSION?></div>
		<strong><?php echo CI_COPYRIGHT?></strong>		
	  </footer>	  
	  <div class="control-sidebar-bg"></div>
	</div>
	<!-- jQuery 2.2.3 -->
	<script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="<?=base_url('assets/js/jquery-ui.min.js')?>"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="<?=base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script>
	<!-- Highcharts.js charts -->	
	<script src="<?=base_url('assets/js/highcharts/highcharts.js')?>"></script>
	<script src="<?=base_url('assets/js/highcharts/exporting.js')?>"></script>
	<!-- Morris.js charts -->
	<script src="<?=base_url('assets/js/raphael-min.js')?>"></script>
	<script src="<?=base_url('assets/plugins/morris/morris.min.js')?>"></script>
	<!-- Sparkline -->
	<script src="<?=base_url('assets/plugins/sparkline/jquery.sparkline.min.js')?>"></script>
	<!-- jvectormap -->
	<script src="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
	<script src="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
	<!-- jQuery Knob Chart -->
	<script src="<?=base_url('assets/plugins/knob/jquery.knob.js')?>"></script>
	<!-- daterangepicker -->
	<script src="<?=base_url('assets/js/moment.min.js')?>"></script>
	<script src="<?=base_url('assets/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
	<!-- fullCalendar 2.2.5  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
	<script src="<?=base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
	<!-- datepicker -->
	<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js')?>"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
	<!-- Slimscroll -->
	<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
	<!-- FastClick -->
	<script src="<?=base_url('assets/plugins/fastclick/fastclick.js')?>"></script>
	<!-- AdminLTE App -->
	<script src="<?=base_url('assets/js/app.min.js')?>"></script>
	<!-- Dialog Bootstrap -->
	<script src="<?=base_url('assets/js/bootstrap/bootstrap-dialog.min.js')?>"></script>
    <!-- Bootstrap Table -->
    <script src="<?=base_url('assets/plugins/bootstrap-table/bootstrap-table.js')?>"></script>
    <script src="<?=base_url('assets/plugins/bootstrap-table/bootstrap-table-pt-BR.js')?>"></script>
	<!-- Bootbox -->
	<script src="<?=base_url('assets/plugins/bootbox/bootbox.js')?>"></script>
	<!-- jQuery Validate -->
	<script src="<?=base_url('assets/plugins/jquery-validate/jquery.validate.min.js')?>"></script>
	<script src="<?=base_url('assets/plugins/jquery-validate/jquery.validate.messages.js')?>"></script>
	<!-- Mask Edit Input -->
	<script src="<?=base_url('assets/js/jquery.maskedinput.2015.js')?>"></script>
	<!-- Mask Edit Input -->
	<script src="<?=base_url('assets/js/jquery.maskMoney.js')?>"></script>
	<!-- Select2 -->
	<script src="<?=base_url('assets/plugins/select2/bootstrap-select.js')?>"></script>
	<!-- MultiSelect -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css')?>" type="text/css">
    <script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js')?>"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?=base_url('assets/js/demo.js')?>"></script>
	<script src="<?=base_url('assets/js/comum.js')?>"></script>
	<script src="<?=base_url('assets/js/init.js')?>"></script>
</body>
</html>