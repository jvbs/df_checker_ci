<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <title><?php echo CI_TITULO_PAGINA;?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="description" content="<?php echo CI_DESCRICAO_PAGINA;?>"/>
	<meta name="author" content="<?php echo CI_AUTOR_PAGINA;?>">
	<meta name="keywords" content="<?php echo CI_PALAVRA_CHAVE_PAGINA?>">	
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css')?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="<?php echo CI_FAVICON?>"/>
</head>
<body>