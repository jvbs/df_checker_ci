<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <title><?php echo CI_TITULO_PAGINA;?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php echo CI_DESCRICAO_PAGINA;?>">
  <meta name="author" content="<?php echo CI_AUTOR_PAGINA;?>">
  <meta name="keywords" content="<?php echo CI_PALAVRA_CHAVE_PAGINA?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('assets/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url('assets/css/skins/_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/flat/blue.css')?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/morris/morris.css')?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/daterangepicker/daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap/bootstrap-dialog.min.css')?>">
  <!-- Dialog Bootstrap -->
  <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <!-- DataTables
  <link rel="stylesheet" href="< ?=base_url('assets/plugins/datatables/dataTables.bootstrap.css')?>">
  Tabs -->
  <!-- FancyBox
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fancybox/css/fancybox/jquery.fancybox-buttons.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fancybox/css/fancybox/jquery.fancybox-thumbs.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fancybox/css/fancybox/jquery.fancybox-thumbs.css')?>">
  -->
  <!-- Bootstrap Table CSS -->
  <link href="<?=base_url('assets/plugins/bootstrap-table/bootstrap-table.css')?>" rel="stylesheet">
  <!--
  <link rel="stylesheet" href="< ?=base_url('assets/css/bootstrap.tabs.css')?>">
  -->

  <!-- FullCalendar -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fullcalendar/fullcalendar.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fullcalendar/fullcalendar.print.css')?>" media="print">
  <!-- Select2 -->
  <link href="<?=base_url('assets/plugins/select2/custom.css')?>"/>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/bootstrap-select.css')?>">
  <link rel="shortcut icon" href="<?php echo CI_FAVICON?>"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<input type="hidden" id="hddCarregando" value="<?php echo CI_CARREGANDO;?>"/>
<input type="hidden" id="hddCarregandoCor" value="<?php echo CI_CARREGANDO_COR;?>"/>
<input type="hidden" id="hddSelecione" value="<?php echo CI_SELECIONE;?>"/>
<input type="hidden" id="hddNovo" value="<?php echo CI_NOVO;?>"/>
<input type="hidden" id="hddEnviando" value="<?php echo CI_ENVIANDO;?>"/>
<input type="hidden" id="hddHome" value="<?php echo base_url('home');?>"/>
<input type="hidden" id="hddEnviarMensagem" value="<?php echo base_url('mensagens_internas_salvar');?>"/>
<input type="hidden" id="hddMensagensInternasDados" value="<?php echo base_url('mensagens_internas_consultar_dados');?>"/>
<input type="hidden" id="hddCarregarFormularios" value="<?php echo base_url('users_forms_loading');?>"/>
<input type="hidden" id="hddFadeOut" value="<?php echo CI_FADEOUT;?>"/>
<input type="hidden" id="hddInfoAlert" value="<?php echo CI_INFO_ALERT;?>"/>
<input type="hidden" id="hddInfoError" value="<?php echo CI_INFO_ERROR;?>"/>
<input type="hidden" id="hddSetTimeOut" value="<?php echo CI_SETTIMEOUT;?>"/>
<input type="hidden" id="hddSetInterval" value="<?php echo CI_SETINTERVAL;?>"/>
<input type="hidden" id="hddTituloPagina" value="<?php echo CI_TITULO_PAGINA;?>"/>
<input type="hidden" id="hddMensagemID" value="">
<input type="hidden" id="hddTimeOutID" value=""/>
<input type="hidden" id="hddTelaAcao" value=""/>
<input type="hidden" id="hddTelaNome" value=""/>
<input type="hidden" id="hddFormularioSelecionado" value=""/>
<input type="hidden" id="hddFormularioID" value=""/>
<input type="hidden" id="hddMobile" value="<?php echo $strMobile?>"/>
<input type="hidden" id="hddDiretorioUsuarios" value="<?php echo CI_DIRETORIO_USUARIOS?>"/>
<input type="hidden" id="hddExcluir" value=""/>
<input type="hidden" id="hddCodigoSelecionado" value=""/>
<input type="hidden" id="hddCarregar" value=""/>
<input type="hidden" id="hddAnoAtual" value="<?php echo date('Y')?>"/>
<input type="hidden" id="hddMesAtual" value="<?php echo date('m')?>"/>
<input type="hidden" id="hddDiaAtual" value="<?php echo date('d')?>"/>
<input type="hidden" id="hddFlagPessoaFisica" value="<?php echo CI_FLAG_PESSOA_FISICA?>"/>
<input type="hidden" id="hddFlagPessoaJuridica" value="<?php echo CI_FLAG_PESSOA_JURIDICA?>"/>
<input type="hidden" id="hddCPF" value="<?php echo CI_LABEL_CPF?>"/>
<input type="hidden" id="hddLabelCNPJ" value="<?php echo CI_LABEL_CNPJ?>"/>
<input type="hidden" id="hddAcaoConsultarDados" value="<?php echo base_url('acoes_consultar_dados')?>"/>
<input type="hidden" id="hddLabelConfirmarNome" value="<?php echo CI_LABEL_CONFIRMAR_NOME?>"/>
<input type="hidden" id="hddTituloExcluir" value="<?php echo CI_TITLE_EXCLUIR?>"/>
<input type="hidden" id="hddLabelBtnCadastrar" value="<?php echo CI_LABEL_BTN_CADASTRAR?>"/>
<input type="hidden" id="hddLabelBtnCancelar" value="<?php echo CI_LABEL_BTN_CANCELAR?>"/>
<input type="hidden" id="hddSemDados" value="<?php echo CI_SEM_DADOS?>"/>
<input type="hidden" id="hddCarregarGraficoUsuarios" value="<?php echo base_url('users_generate_graphics')?>"/>
<input type="hidden" id="hddServiceDetailNews" value="<?php echo base_url('servicedetail_news')?>"/>
<input type="hidden" id="hddServiceDetailLists" value="<?php echo base_url('servicedetail_lists')?>"/>
<input type="hidden" id="hddServiceDetailSave" value="<?php echo base_url('servicedetail_save')?>"/>
<input type="hidden" id="hddLabelGraficoUsuarios" value="<?php echo CI_LABEL_GRAFICO_USUARIOS?>"/>
<input type="hidden" id="hddSigla" value="<?php echo CI_SIGLA?>"/>
<div style="z-index: 1000000;" class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel"><?php echo CI_LABEL_CONFIRMAR_EXCLUSAO?></h4>
			</div>
			<div class="modal-body">
				<p id="descricaoExcluir"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo CI_LABEL_CONFIRMAR_CANCELAR?></button>
				<button id="linkExcluir" type="button" class="btn btn-primary" data-dismiss="modal" onClick="excluir($.trim($('#hddExcluir').val()))"><?php echo CI_LABEL_CONFIRMAR_OK?></button>
			</div>
		</div>
	</div>
</div>
<div class="wrapper">
  <header class="main-header">
    <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');" onClick="redir('<?php echo base_url('home');?>');" class="logo">
      <span class="logo-mini"><b><?php echo CI_NOME_PROJETO;?></b></span>
      <span class="logo-lg"><b><?php echo CI_NOME_PROJETO;?></b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a id="menuTopo" href="javascript: void('<?php echo CI_NOME_PROJETO?>');" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');" class="dropdown-toggle" data-toggle="dropdown">
			  <?php if (is_url_exist(CI_DIRETORIO_USUARIOS.$this->session->userdata('session_USU_Imagem'))){ ?>
				<img src="<?php echo CI_DIRETORIO_USUARIOS.$this->session->userdata('session_USU_Imagem')?>" class="user-image">
			  <?php } ?>
              <span class="hidden-xs"><?php echo $this->session->userdata('session_USU_Nome'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
				<?php if (is_url_exist(CI_DIRETORIO_USUARIOS.$arrDados[0]['USU_Imagem']) && !empty($this->session->userdata('session_USU_Imagem'))){ ?>
				<img src="<?php echo CI_DIRETORIO_USUARIOS.$this->session->userdata('session_USU_Imagem')?>" class="img-circle">
				<?php } ?>
                <p>
                  <?php echo $this->session->userdata('session_USU_Nome')?>
                  <small>Membro desde <?php echo exibirMesAnoData($this->session->userdata('session_USU_DataCadastro'))?></small>
                </p>
              </li>
              <li class="user-body" style="display:none;">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');">Seguidores</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');">Vendas</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');">Amigos</a>
                  </div>
                </div>
              </li>
              <li class="user-footer">
                <div class="pull-left" style="display:none;">
                  <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');" onClick="alterar('<?php echo base_url('usuarios_editar/'.$this->session->userdata('session_USU_ID'))?>');" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url($this->router->routes['users_logout'])?>" onClick="" class="btn btn-default btn-flat">Sair</a>
                </div>
              </li>
            </ul>
          </li>
          <li>
            <a href="javascript: void('<?php echo CI_NOME_PROJETO?>');" data-toggle="control-sidebar">
				<i class="fa fa-gears"></i>
			</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" id="ulFormularios"></ul>
    </section>
  </aside>
    <div class="content-wrapper">
    <section class="content-header">
      <h1 id="nomePagina"><?=$strPagina?></h1>
      <ol class="breadcrumb">
        <li>
			<a href="javascript: void('<?php echo CI_NOME_PROJETO?>');">
				<i class="fa fa-dashboard"></i> <?php echo CI_LABEL_INICIO?>
			</a>
		</li>
        <li class="active" id="nomeNavegacao"><?php echo $strPagina?></li>
      </ol>
	  <?php if ($this->session->userdata('error_crud')): ?>
		<div id="div-warning-home" class="alert alert-warning"><?php echo $this->session->userdata('error_crud'); ?></div>
	  <?php $this->session->unset_userdata('error_crud'); endif; ?>
	  <?php if ($this->session->userdata('danger_crud')): ?>
		<div id="div-danger-home" class="alert alert-danger"><?php echo $this->session->userdata('danger_crud'); ?></div>
	  <?php $this->session->unset_userdata('danger_crud'); endif; ?>
	  <?php if ($this->session->userdata('success_crud')): ?>
		<div id="div-success-home" class="alert alert-success"><?php echo $this->session->userdata('success_crud'); ?></div>
	  <?php $this->session->unset_userdata('success_crud'); endif; ?>
	  <div id="div-success-delete" class="alert alert-success" style="display:none;"></div>
    </section>
