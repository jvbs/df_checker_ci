<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

  // Variável que define o nome da tabela
  var $table = "";

  /**
  * Método Construtor
  */
  public function __construct() {
    parent::__construct();
  }

  /**
  * Insere um registro na tabela
  *
  * @param array $data Dados a serem inseridos
  *
  * @return boolean
  */
  public function Insert($data) {
    if(!isset($data)) return false;
	//echo '<pre>'; print_r($data); exit('333333');
	return $this->db->insert($this->table, $data);
  }

  /**
  * Recupera um registro a partir de um ID
  *
  * @param integer $id ID do registro a ser recuperado
  *
  * @return array
  */
  public function GetById($field, $id) {
    if( is_null($field) || is_null($id)) return false;

    $this->db->where($field, $id);
    $query = $this->db->get($this->table);

    if ($query->num_rows() > 0) {
      return $query->row_array();
    }else{
      return null;
    }
  }

  /**
  * Lista todos os registros da tabela
  *
  * @param string $sort Campo para ordenação dos registros
  *
  * @param string $order Tipo de ordenação: ASC ou DESC
  *
  * @return array
  */
  public function GetAll($filters = null, $orderby = null, $likes = null, $whereor = null){ //, $orderby = null
	//where
	if (is_array($filters)){
		foreach ($filters as $key => $value){
			$this->db->where($key, $value);
		}
	}

	if (is_array($whereor)){
		foreach ($whereor as $key => $value){
			if (count($value) > 0){
				$intI = 0;
				foreach($value as $v){
					if ($intI == 0){
						$this->db->where($key, $v);
					}else{
						$this->db->or_where($key, $v);	
					}
					
					$intI++;
				}
			}
		}
	}

	//orderby
	if (is_array($orderby)){
		foreach ($orderby as $key => $value){
			$this->db->order_by($key, $value);
		}
	}

	//like
	if (is_array($likes)){
		foreach($likes as $key => $value){
			$this->db->like($key, $value);			
		}
	}

    $query = $this->db->get($this->table);
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }else{
      return null;
    }
  }

  /**
  * Atualiza um registro na tabela
  *
  * @param integer $int ID do registro a ser atualizado
  *
  * @param array $data Dados a serem inseridos
  *
  * @return boolean
  */
  public function Update($fieldID, $data){	  
	if (is_array($fieldID)){
		$intI  = 0;
		$chave = null;
		$valor = null;
		foreach ($fieldID as $key => $value){
			$this->db->where($key, $value);

			if ($intI == 0){				
				$chave = $key;
				$valor = $value;
			}

			$intI++;
		}

		if ($this->db->update($this->table, $data)){
			if (is_array($data)){
				return true;
			}
		}
	}else{
		return false;
	}
  }

  /**
  * Remove um registro na tabela
  *
  * @param integer $int ID do registro a ser removido
  *
  *
  * @return boolean
  */
  public function Remove($array){
	if (is_array($array) && count($array) > 0){
		foreach($array as $key => $value){

			$this->db->where($key, $value);
		}
		return $this->db->delete($this->table);
	}
	return false;
  }
    
  public function InsertId($data) {
    if(!isset($data)) return false;	
	$this->db->insert($this->table, $data);	
	return $this->db->insert_id();
  }
}