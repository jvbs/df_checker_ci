<?php
class ServiceOrdersDAO extends MY_Model{
    public function __construct(){
        parent::__construct();
        $this->table = 'service_orders';
    }

	public function ServiceOrdersLists($arrFiltros = null){
		$strSQL = " SELECT * from service_orders a
                    where status = 'A'";

		if(isset($arrFiltros['user_id'])){
			$strSQL.= " AND a.user_id = ".$arrFiltros['user_id'];
		}

		$strSQL.= " ORDER BY a.id DESC";

		if(isset($arrFiltros['debug'])) exit($strSQL);

		if($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}

	public function HomeServiceOrdersInfo($arrFiltros = null){
		$strSQL = "	SELECT 
						sum(case when service_order_status = 'Finalizada' then 1 else 0 end) as execucoes,
						sum(case when service_order_status <> 'Finalizada' then 1 else 0 end) as pendentes,
						sum(case when month(service_order_date) = month(now()) then 1 else 0 end*case when service_order_status = 'Finalizada' then 1 else 0 end) as execucoes_mes
					FROM checker_ci.service_orders";
		if(isset($arrFiltros['user_id'])){
			$strSQL.= " where user_id = ".$arrFiltros['user_id'];
		}
		if(isset($arrFiltros['analyst'])){
			$strSQL.= " where analyst = ".$arrFiltros['analyst'];
		}
		if($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}

	public function HomeServiceOrdersChartStatus($arrFiltros = null){
		$strSQL = "	SELECT 
						service_order_status,
						count(*) as qtd
					FROM checker_ci.service_orders";
		if(isset($arrFiltros['user_id'])){
			$strSQL.= " where user_id = ".$arrFiltros['user_id'];
		}
		$strSQL.="	group by service_order_status
					order by service_order_status";
		if($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}

	public function HomeServiceOrdersChartExecutions($arrFiltros = null){
		$strSQL = "	SELECT 	concat('Semana/', week(service_order_date))  as semana,
							count(*) as qtd
					from service_orders
					where service_order_status = 'Finalizada'";
		if(isset($arrFiltros['user_id'])){
			$strSQL.= " AND user_id = ".$arrFiltros['user_id'];
		}
		$strSQL.= " group by concat('Semana/', week(service_order_date))";
		if($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}
}
