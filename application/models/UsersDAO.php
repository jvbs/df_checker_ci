<?php
class UsersDAO extends MY_Model{
    public function __construct(){
        parent::__construct();
        $this->table = 'users';
    }

    public function CarregaUsuariosLogin($arrayFilters = null){
        $sql = "SELECT a.*, b.permissions from users a
                inner join permissions b
                on a.user_level = b.id
                where status = 'A' and company_id = '{$arrayFilters["company_id"]}' and password = '{$arrayFilters["password"]}'";

        if(isset($arrayFilters["company_id"]) && isset($arrayFilters["password"])){
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query->result_array();
            }
        }
    }

	public function CarregarGraficoUsuariosHome($arrFiltros = null){
		$strSQL = "SELECT MONTH(created_at) AS mesCadastro, YEAR(created_at) AS anoCadastro, COUNT(id) AS TOTAL FROM users WHERE id IS NOT NULL ";

		if (isset($arrFiltros['USU_Ano'])){
			$strSQL.= " AND YEAR(created_at) = ".$arrFiltros['USU_Ano'];
		}

		if (isset($arrFiltros['USU_Mes'])){
			$strSQL.= " AND MONTH(created_at) = ".$arrFiltros['USU_Mes'];
		}

		$strSQL.= " GROUP BY MONTH(created_at), YEAR(created_at) ";
		$strSQL.= "ORDER BY MONTH(created_at), YEAR(created_at)";

		if (isset($arrFiltros['debug'])) exit($strSQL);

		if ($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}

		return false;
	}



	

	public function ConsultarUsuarios($arrFiltros = null){
		if (is_null($arrFiltros)) return false;

		$strSQL = "SELECT * FROM users WHERE id IS NOT NULL ";

		if (isset($arrFiltros['USU_IDDiferente'])){
			$strSQL.= " AND id <> ".$arrFiltros['USU_IDDiferente'];
		}

		if (isset($arrFiltros['USU_Email'])){
			$strSQL.= " AND email = '".$arrFiltros['USU_Email']."'";
		}

		if (isset($arrFiltros['USU_Status'])){
			$strSQL.= " AND status = '".$arrFiltros['USU_Status']."'";
		}

		if ($rsQuery = $this->db->query($strSQL)){
            if ($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}

		return false;
	}
}
