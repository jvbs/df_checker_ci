<?php
class QueryListDAO extends MY_Model{
    public function __construct(){
        parent::__construct();
        $this->table = 'query_list';
    }

    public function QueryLists($arrFiltros = null){
		$strSQL = " SELECT   *
                    FROM query_list
                    WHERE status = 'A'";

		if(isset($arrFiltros['queries'])){
			$strSQL.= " AND id in ({$arrFiltros['queries']})";
		}

		if (isset($arrFiltros['debug'])) exit($strSQL);

		if ($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}
}
