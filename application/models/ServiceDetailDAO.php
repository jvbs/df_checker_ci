<?php
class ServiceDetailDAO extends MY_Model{
    public function __construct(){
        parent::__construct();
        $this->table = 'service_order_details';
    }

	public function ServiceDetailLists($arrFiltros = null){
		$strSQL = " SELECT   a.*,
                             b.service_order_status,
                             b.service_order_deadline,
                             c.fantasy_name,
                             d.name,
                             e.report_name,
                             e.queries
                    FROM service_order_details a
                    INNER JOIN service_orders b
                    ON a.service_order_id = b.service_order_id
                    INNER JOIN clients c
                    ON a.client_id = c.id
                    INNER JOIN users d
                    ON a.analyst = d.id
                    INNER JOIN reports e
                    ON a.report_id = e.id
                    WHERE d.user_level = 2
                    AND a.status = 'A'
                    AND b.status = 'A'
                    AND c.status = 'A'
                    AND d.status = 'A'
                    AND e.status = 'A' ";

		if(isset($arrFiltros['user_id'])){
			$strSQL.= " AND d.id = ".$arrFiltros['user_id'];
		}

        if(isset($arrFiltros['id'])){
			$strSQL.= " AND a.id = ".$arrFiltros['id'];
		}

		$strSQL.= " ORDER BY b.service_order_deadline";

		if (isset($arrFiltros['debug'])) exit($strSQL);

		if ($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}

		return false;
	}
}
