<?php
class ReportDesignerDAO extends MY_Model{
    public function __construct(){
        parent::__construct();
        $this->table = 'reports';
    }

	public function ReportsLists($arrFiltros = null){
		$strSQL = " SELECT a.*, b.fantasy_name, c.nickname
                    FROM reports a
            		INNER JOIN clients b
                    ON a.client_id = b.id
                    INNER JOIN users c
                    ON a.user_id = c.id
                    WHERE a.status = 'A'";

		if(isset($arrFiltros['id'])){
			$strSQL.= " AND a.id = ".$arrFiltros['id'];
		}

		$strSQL.= " ORDER BY a.id";

		if(isset($arrFiltros['debug'])) exit($strSQL);

		if($rsQuery = $this->db->query($strSQL)){
            if($rsQuery->num_rows() > 0){
                return $rsQuery->result_array();
            }
		}
		return false;
	}
}
